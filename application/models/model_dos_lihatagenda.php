<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_dos_lihatagenda extends CI_Model
{
	public $db_tabel 	= 'view_agenda';
	public $per_halaman	= 10;
	public $offset 		= 0;

	public function cari_semua($offset)
	{
		$dosen = $this->session->userdata('users_name');
		
		if (is_null($offset) || empty($offset))
		{
			$this->offset = 0;
		}
		else
		{
			$this->offset = ($offset * $this->per_halaman) - $this->per_halaman;
		}
		
		return $this->db->select('*')
						->from($this->db_tabel)
						->where('nip',$dosen)
						->where('tanggal > now()')
						->order_by('tanggal','desc')
						->limit($this->per_halaman, $this->offset)
						->get()
						->result();
	}
	
	public function buat_tabel($data)
	{
		$this->load->library('table');
		$this->table->set_heading('Tanggal','Mahasiswa','Ruangan','Jam Mulai','Kegiatan');
		
		foreach($data as $row)
		{
			 $hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->tanggal));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->tanggal));
            $hr_tgl = "$hari, $tgl";
			
			$this->table->add_row(
				$hr_tgl,
				$row->nama_mahasiswa,
				$row->ruangan,
				$row->jam_mulai,
				$row->kegiatan
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function paging($base_url)
	{
		$this->load->library('pagination');
		$config = array(
			'base_url'         => $base_url,
            'total_rows'       => $this->hitung_semua(),
            'per_page'         => $this->per_halaman,
            'num_links'        => 4,
            'use_page_numbers' => TRUE,
            'first_link'       => '&#124;&lt; First',
            'last_link'        => 'Last &gt;&#124;',
            'next_link'        => 'Next &gt;',
            'prev_link'        => '&lt; Prev',
		);
		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}
	
	public function hitung_semua()
	{
		$dosen = $this->session->userdata('users_name');
		
		return $this->db->select('*')
						->from($this->db_tabel)
						->where('nip',$dosen)
						->where('tanggal > now()')
						->get()
						->num_rows();
	}
}