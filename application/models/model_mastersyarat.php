<?php if(!defined ('BASEPATH')) exit ('No direct script acces allowed');

class Model_mastersyarat extends CI_Model
{
	public $db_tabel		= 'mdoksyarat';
	public $per_halaman		= 10;
	public $offset			= 0;
	
	private function load_form_rules_tambah()
	{
		$form = array(
					array(
					'label'	=> 'Nama Dokumen',
					'field'	=> 'doksyarat_nama',
					'rules'	=> 'required'
					),
					
					array(
					'label'	=> 'Format File Upload',
					'field'	=> 'format_file_upload',
					'rules'	=> 'required'
					),
					array(
					'label'	=> 'Keterangan',
					'field'	=> 'keterangan',
					
					),
					array(
					'label'	=> 'Cek Dosen',
					'field'	=> 'cekdosen',
					
					),
					array(
					'label'	=> 'Kode Syarat',
					'field'	=> 'kode_ss',
					'rules'	=> 'required'
					),
		);
		return $form;
	}
	
	private function load_form_edit()
	{
		$form = array(
					array(
					'label'	=> 'Nama Dokumen',
					'field'	=> 'doksyarat_nama',
					'rules'	=> 'required'
					),
					
					array(
					'label'	=> 'Format File Upload',
					'field'	=> 'format_file_upload',
//					'rules'	=> 'required'
					),
					array(
					'label'	=> 'Keterangan',
					'field'	=> 'keterangan',
					
					),
					array(
					'label'	=> 'Cek Dosen',
					'field'	=> 'cekdosen',
					
					),
					array(
					'label'	=> 'Kode Syarat',
					'field'	=> 'kode_ss',
					'rules'	=> 'required'
					),
		);
		return $form;
	}
	
	public function validasi_tambah()
	{
		$form = $this->load_form_rules_tambah();
		$this->form_validation->set_rules($form);
		
		if($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function validasi_edit()
	{
		$form = $this->load_form_edit();
		$this->form_validation->set_rules($form);
		
		if($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function cari_semua($offset)
	{
		if (is_null($offset) || empty ($offset))
		{
			$this->offset=0;
		}
		else
		{
			$this->offset = ($offset * $this->per_halaman) - $this->per_halaman;
		}
		
		return $this->db->select('*')
						->from($this->db_tabel)
						->limit($this->per_halaman, $this->offset)
						->order_by('idmDokSyarat','ASC')
						->get()
						->result();
	}
	
	public function cari($idmDokSyarat)
	{
		return $this->db->where('idmDokSyarat', $idmDokSyarat)
				->limit(1)
				->get($this->db_tabel)
				->row();
	}
	
	public function cek($cek)
	{
		if($cek == '1')
		{
			$cek = 'Yes';
			return $cek;
		}
		else
		{
			$cek = 'No';
			return $cek;
		}
	}
	
	public function buat_tabel($data)
	{
		$this->load->library('table');
		 $tmpl = array('row_alt_start'  => '<tr class="zebra">');
        $this->table->set_template($tmpl);

		$this->table->set_heading('Nama Dokumen','Format File Upload','Keterangan','Validasi Dosen','kode syarat','Aksi');
		foreach($data as $row)
		{
			
			$cekdosen = $this->cek($row->cekdosen);
			
			$this->table->add_row(
			$row->doksyarat_nama,
			$row->format_file_upload,
			$row->keterangan,
			$cekdosen,
			$row->kode_ss,
			anchor('master_syarat/edit/'.$row->idmDokSyarat,'Edit',array('class' => 'edit')).' '.
			anchor('master_syarat/hapus/'.$row->idmDokSyarat,'Hapus',array('class' => 'delete','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')"))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function paging($base_url)
	{
		$this->load->library('pagination');
		$config = array(
				'base_url'			=> $base_url,
				'total_rows'		=> $this->hitung_semua(),
				'per_page'			=> $this->per_halaman,
				'num_links'			=> 2,
				'use_page_numbers'	=> TRUE,
	            'first_link'       => '&#124;&lt; First',
            	'last_link'        => 'Last &gt;&#124;',
	            'next_link'        => 'Next &gt;',
    	        'prev_link'        => '&lt; Prev',
		);
		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}
	
	public function hitung_semua()
	{
		return $this->db->count_all($this->db_tabel);
	}
	
	public function tambah()
	{
		$data = array(
			'doksyarat_nama'		=> $this->input->post('doksyarat_nama'),
			'format_file_upload'	=> $this->input->post('format_file_upload'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'cekdosen'				=> $this->input->post('cekdosen'),
			'kode_ss'				=> $this->input->post('kode_ss'),
		);
		$this->db->insert($this->db_tabel, $data);
		
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function edit($idmDokSyarat)
	{
		$data = array(
			'doksyarat_nama'		=> $this->input->post('doksyarat_nama'),
			'format_file_upload'	=> $this->input->post('format_file_upload'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'cekdosen'				=> $this->input->post('cekdosen'),
			'kode_ss'				=> $this->input->post('kode_ss'),
		);
		$this->db->where('idmDokSyarat', $idmDokSyarat);
		$this->db->update($this->db_tabel, $data);
		
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function hapus($idmDokSyarat)
	{
		$this->db->where('idmDokSyarat', $idmDokSyarat)->delete($this->db_tabel);
		
		if($this->db->affected_rows()>0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
}
/* End of file model_MasterSyarat.php */
/* Location: ./application/models/model_MasterSyarat.php */