<?php
class Model_adm_nilaiseminar extends CI_Model {

    public $db_tabel    = 'tsemsid';
	public $db_sk		= 'skripsi';
	public $db_status	= 'mstatusskrip';
    public $per_halaman = 10;
    public $offset      = 0;

    private function load_form_rules_edit()
    {
        $form = array(
            array(
                'field' => 'nilai_bimb1',
                'label' => 'Nilai Pembimbing 1',
                'rules' => 'required'
            ),
            array(
                'field' => 'nilai_bimb2',
                'label' => 'Nilai Pembimbing 2',
                'rules' => 'required'
            ),
        );
        return $form;
    }

    public function validasi_edit()
    {
        $form = $this->load_form_rules_edit();
        $this->form_validation->set_rules($form);

        if ($this->form_validation->run())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

	public function cari_semua($offset)
	{
        if (is_null($offset) || empty($offset))
        {
            $this->offset = 0;
        }
        else
        {
            $this->offset = ($offset * $this->per_halaman) - $this->per_halaman;
        }
        // $offset end

		return $this->db->select('mmhsw.mhsw_nama,
									skripsi.skrip_judul,
									tsemsid.nilai_bimb1,
									tsemsid.nilai_bimb2,
									tsemsid.lockedit,
									tsemsid.idtSemSid,
									tsemsid.semsid_tgl,
									tsemsid.idmSkripsi'
									)
								->from('
								mmhsw,
								skripsi,
								tsemsid,
								mkgtn
								')
								->where('mmhsw.idmmhsw = skripsi.idmMhsw')
								->where('skripsi.idmSkripsi = tsemsid.idmSkripsi')
								->where('mkgtn.idmKgtn = tsemsid.idmKgtn')
								->where('tsemsid.lockedit = 0')
								->where('tsemsid.idmKgtn` = 1')
                        ->order_by('tsemsid.semsid_tgl', 'asc')
                        ->limit($this->per_halaman, $this->offset)
                        ->get()->result();
	}
	
	public function cari_id($id)
	{
       return $this->db->select('mmhsw.mhsw_nama,
									skripsi.skrip_judul,
									tsemsid.nilai_bimb1,
									tsemsid.nilai_bimb2,
									tsemsid.lockedit,
									tsemsid.idtSemSid,
									tsemsid.semsid_tgl,
									tsemsid.idmSkripsi'
									)
								->from('
								mmhsw,
								skripsi,
								tsemsid,
								mkgtn
								')
								->where('mmhsw.idmmhsw = skripsi.idmMhsw')
								->where('skripsi.idmSkripsi = tsemsid.idmSkripsi')
								->where('mkgtn.idmKgtn = tsemsid.idmKgtn')
								->where('tsemsid.idtSemSid',$id)
								->where('tsemsid.lockedit = 0')
								->where('tsemsid.idmKgtn` = 1')
								->get()->result();
	}
	
    public function cari($idtSemSid)
    {
        return $this->db->where('idtSemSid', $idtSemSid)
            ->limit(1)
            ->get($this->db_tabel)
            ->row();
    }

    public function buat_tabel($absen)
    {
        $this->load->library('table');

        // Buat class zebra di <tr>,untuk warna selang-seling
        $tmpl = array('row_alt_start'  => '<tr class="zebra">');
        $this->table->set_template($tmpl);

        /// Buat heading tabel
        $this->table->set_heading('Nama', 'Judul Skripsi', 'tanggal','Nilai Pembimbing 1', 'Nilai Pembimbing 2');

        foreach ($absen as $row)
        {
             $hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->semsid_tgl));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->semsid_tgl));
            $hr_tgl = "$hari, $tgl";

			$this->table->add_row(
				$row->mhsw_nama,
				$row->skrip_judul,
				$hr_tgl,
				$row->nilai_bimb1,
				$row->nilai_bimb2,
				anchor('adm_nilaiseminar/edit/'.$row->idtSemSid,'Entri',array('class' => 'baru'))
            );
        }
        $tabel = $this->table->generate();

        return $tabel;
    }

    public function paging($base_url)
    {
        $this->load->library('pagination');
        $config = array(
            'base_url'         => $base_url,
            'total_rows'       => $this->hitung_semua(),
            'per_page'         => $this->per_halaman,
            'num_links'        => 2,
            'use_page_numbers' => TRUE,
            'first_link'       => '&#124;&lt; First',
            'last_link'        => 'Last &gt;&#124;',
            'next_link'        => 'Next &gt;',
            'prev_link'        => '&lt; Prev',
        );
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

	public function hitung_semua()
	{
		return $this->db->select('mmhsw.mhsw_nama,
									skripsi.skrip_judul,
									tsemsid.nilai_bimb1,
									tsemsid.nilai_bimb2,
									tsemsid.lockedit,
									tsemsid.idtSemSid,
									tsemsid.semsid_tgl')
								->from('
								mmhsw,
								skripsi,
								tsemsid,
								mkgtn
								')
								->where('mmhsw.idmmhsw = skripsi.idmMhsw')
								->where('skripsi.idmSkripsi = tsemsid.idmSkripsi')
								->where('mkgtn.idmKgtn = tsemsid.idmKgtn')
								->where('tsemsid.lockedit = 0')
								->where('tsemsid.idmKgtn` = 1')
								->order_by('tsemsid.semsid_tgl', 'desc')
								->get()->num_rows();
	}

	public function edit($idtSemSid)
    {
        $nilai = array(
            'nilai_bimb1'=>$this->input->post('nilai_bimb1'),
            'nilai_bimb2'=>$this->input->post('nilai_bimb2')
        );

        // update db
        $this->db->where('idtSemSid', $idtSemSid)->update($this->db_tabel, $nilai);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
	
	public function edit_status($id)
	{
		$edit = array(
			'idmStatusPros' => 4
		);
		$this->db->where('idmSkripsi',$id)->update($this->db_sk, $edit);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		
	}
	
	public function status_skripsi($id)
    {
        $status = array(
            'idmStatusSkrip'=>$this->input->post('idmStatusSkrip')
        );

        $this->db->where('idmSkripsi',$id)->update($this->db_sk, $status);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
	
	public function SS()
	{
		return $this->db->select('*')
						->from($this->db_status)
						->where('idmStatusSkrip < 5')
						->get()
						->result();
	}


}
/* End of file model_adm_nilaiseminar.php */
/* Location: ./application/models/model_adm_nilaiseminar.php */