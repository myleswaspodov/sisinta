<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_login extends CI_Model {

    public $db_tabel = 'users';
	public $db_tabel2 = 'mdosen';
	public $user = 'users_name';
	public $jeneng;
	
    public function load_form_rules()
    {
        $form_rules = array(
                            array(
                                'field' => 'users_name',
                                'label' => 'Username',
                                'rules' => 'required'
                            ),
                            array(
                                'field' => 'users_passd',
                                'label' => 'Password',
                                'rules' => 'required'
                            ),
        );
        return $form_rules;
    }
	
	public function load_form_ganti_pass()
	{
		$form = array(
			array(
					'field' => 'users_passd',
					'label' => 'Password Baru',
					'rules' => 'required|md5'
				),
		);
		return $form;
	}
	
	public function validasi_gantipassword()
	{
		$form = $this->load_form_ganti_pass();
		$this->form_validation->set_rules($form);
		
		if($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function ganti_password($users_name)
	{
		$pass_baru = array(
			'users_passd' => $this->input->post('users_passd')
		);
		$this->db->where('users_name',$users_name);
		$this->db->update($this->db_tabel,$pass_baru);
		
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
    public function validasi()
    {
        $form = $this->load_form_rules();
        $this->form_validation->set_rules($form);

        if ($this->form_validation->run())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function hasil()
	{
		$hasil = $this->session->userdata('users_name');
		
		return $this->db->select('*')
						->from($this->db_tabel)
						->where('users_name',$hasil)
						->get()
						->result();
	}
	
	public function buat_tabel($data)
	{
		$this->load->library('table');
		$this->table->set_heading('User Name','Nama Pengguna');
		
		foreach($data as $row)
		{
			$this->table->add_row(
				$row->users_name,
				$row->user_nama,
				anchor('ganti_password/edit/'.$row->users_name,'Ganti Password',array('class' => 'pass'))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function buat_tabel_a($data)
	{
		$this->load->library('table');
		$this->table->set_heading('User Name','Nama Pengguna');
		
		foreach($data as $row)
		{
			$this->table->add_row(
				$row->users_name,
				$row->user_nama,
				anchor('ganti_password_adm/edit/'.$row->users_name,' Ganti Password',array('class' => 'pass'))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function buat_tabel_d($data)
	{
		$this->load->library('table');
		$this->table->set_heading('User Name','Nama Pengguna');
		
		foreach($data as $row)
		{
			$this->table->add_row(
				$row->users_name,
				$row->user_nama,
				anchor('ganti_password_dos/edit/'.$row->users_name,'Ganti Password',array('class' => 'pass'))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function buat_tabel_k($data)
	{
		$this->load->library('table');
		$this->table->set_heading('User Name','Nama Pengguna');
		
		foreach($data as $row)
		{
			$this->table->add_row(
				$row->users_name,
				$row->user_nama,
				anchor('ganti_password_koor/edit/'.$row->users_name,'Ganti Password',array('class' => 'pass'))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function buat_tabel_kj($data)
	{
		$this->load->library('table');
		$this->table->set_heading('User Name','Nama Pengguna');
		
		foreach($data as $row)
		{
			$this->table->add_row(
				$row->users_name,
				$row->user_nama,
				anchor('ganti_password_kaj/edit/'.$row->users_name,'Ganti Password',array('class' => 'pass'))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	// cek status user, login atau tidak?
    public function cek_user()
    {
        $username = $this->input->post('users_name');
        $password = md5($this->input->post('users_passd'));

        $query = $this->db->where('users_name', $username)
                          ->where('users_passd', $password)
                          ->limit(1)
                          ->get($this->db_tabel);
		
		foreach($query->result() as $result)
		{
			$role = $result->ugrup_idugrup;
			$nama = $result->user_nama;
		}
        if ($query->num_rows() == 1)
        {
            $data = array('users_name' => $username,'ugrup_idugrup'=>$role,'user_nama'=>$nama,'login' => TRUE);
            // buat data session jika login benar
            $this->session->set_userdata($data);
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
	
	public function cek_prodi()
	{
		$id = $this->session->userdata('users_name');
		$DP = $this->db->select('dos_prodi')
						->from($this->db_tabel2)
						->where('dos_nip',$id)
						->get()
						->row()->dos_prodi;
		
		return $this->session->set_userdata('DP',$DP);
	}
	
	public function view_datalogin($users_name)
	{
		$query = $this->db->get_where($this->db_tabel, array($this->user => $users_name));
		return $query->row_array();
	}
	
	public function cari($users_name)
	{
		return $this->db->where('users_name',$users_name)
						->limit(1)	
						->get($this->db_tabel)
						->row();
	}
	
    public function logout()
    {
        $this->session->unset_userdata(array('users_name' => '', 'login' => FALSE));
        $this->session->sess_destroy();
    }
}
/* End of file model_login.php */
/* Location: ./application/models/model_login.php */