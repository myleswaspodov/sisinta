<?php if (!defined('BASEPATH')) exit('No direct script allowed');

class Model_mhs_id extends CI_Model
{
	public $db_tabel = 'view_katalog';
	public $db_sk	= 'skripsi';
	public $db_mhs	= 'mmhsw';
	
	public function info_sendiri($users_name)
	{
		$query = $this->db->get_where($this->db_tabel, array('nim' => $users_name));
		return $query->row_array();
	}
	
	public function cek_data()
	{
		$id = $this->session->userdata('users_name');
		return $this->db->select('*')
						->from($this->db_tabel)
						->where('nim',$id)
						->get()
						->result();
	}
	
	private function form_pengajuan()
	{
		$form = array(
					array(
					'label'	=> 'Judul',
					'field'	=> 'judul',
					'rules'	=> 'required'
					),
					
					array(
					'label'	=> 'Abstrak',
					'field'	=> 'abstrak',
					'rules'	=> 'required'
					),
					
					array(
					'label'	=> 'Dosen Pembimbing 1',
					'field'	=> 'idmDosen_Bi1',
					'rules'	=> 'required'
					),
					
					array(
					'label'	=> 'Dosen Pembimbing 2',
					'field'	=> 'idmDosen_Bi2',
					'rules'	=> 'required'
					),
		);
		return $form;
	}

	private function form_edit()
	{
		$form = array(
					array(
					'label'	=> 'Judul',
					'field'	=> 'judul',
					'rules'	=> 'required'
					),
					
					array(
					'label'	=> 'Abstrak',
					'field'	=> 'abstrak',
					'rules'	=> 'required'
					),
					
					array(
					'label'	=> 'Dosen Pembimbing 1',
					'field'	=> 'idmDosen_Bi1',
					'rules'	=> 'required'
					),
					
					array(
					'label'	=> 'Dosen Pembimbing 2',
					'field'	=> 'idmDosen_Bi2',
					'rules'	=> 'required'
					),
		);
		return $form;
	}
	
	public function validasi_pengajuan()
	{
		$form = $this->form_pengajuan();
		$this->form_validation->set_rules($form);

        if ($this->form_validation->run())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
	}
	
	public function validasi_edit()
	{
		$form = $this->form_edit();
		$this->form_validation->set_rules($form);

        if ($this->form_validation->run())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
	}
	
	public function get_idMHS()
	{
		$id = $this->session->userdata('users_name');
		return $this->db->select('idmmhsw')
						->from($this->db_mhs)
						->where('mhsw_nim',$id)
						->get()
						->row()->idmmhsw;
	}
	
	public function get_idSK()
	{
		$id = $this->get_idMHS();
		return $this->db->select('idmSkripsi')
						->from($this->db_sk)
						->where('idmMhsw',$id)
						->get()
						->row()->idmSkripsi;
	}
	
	public function setor_pengajuan()
	{
		$setor = array(
			'skrip_judul' => $this->input->post('judul'),
			'skrip_abstrak' => $this->input->post('abstrak'),
			'idmDosen_Bi1' => $this->input->post('idmDosen_Bi1'),
			'idmDosen_Bi2' => $this->input->post('idmDosen_Bi2'),
			'idmMhsw' => $this->get_idMHS(),
			'status_judul' => 0,
			'idmStatusPros' => 1,
			'idmStatusSkrip' => 1,
			'skrip_tgl_reg' => date('Y-m-d'),
		);
		$this->db->insert($this->db_sk,$setor);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function setor_baru($id)
	{
		$setor = array(
			'skrip_judul' => $this->input->post('judul'),
			'skrip_abstrak' => $this->input->post('abstrak'),
			'idmDosen_Bi1' => $this->input->post('idmDosen_Bi1'),
			'idmDosen_Bi2' => $this->input->post('idmDosen_Bi2')
		);
		$this->db->where('idmSkripsi',$id)->update($this->db_sk,$setor);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function cari($id)
	{
		return $this->db->select('*')
						->from($this->db_sk)
						->where('idmSkripsi',$id)
						->get()
						->row();
	}
}