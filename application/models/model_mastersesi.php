<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_MasterSesi extends CI_Model {

    private $db_tabel = 'msesi';

	public function __construct()
	{
        parent::__construct();
	}

    public function load_form_rules_tambah()
    {
        $form_rules = array(
            array(
                'field' => 'sesi_mulai',
                'label' => 'Jam Mulai',
                'rules' => "required|is_unique[$this->db_tabel.sesi_mulai]"
            ),
            array(
                'field' => 'sesi_selesai',
                'label' => 'Jam Berakhir',
                'rules' => "required|is_unique[$this->db_tabel.sesi_selesai]"
            ),
        );
        return $form_rules;
    }

    public function load_form_rules_edit()
    {
        $form_rules = array(
            array(
                'field' => 'sesi_mulai',
                'label' => 'Jam Mulai',
                'rules' => "required"
            ),
            array(
                'field' => 'sesi_selesai',
                'label' => 'Jam Selesai',
                'rules' => "required"
            ),
        );
        return $form_rules;
    }

    public function validasi_tambah()
    {
        $form = $this->load_form_rules_tambah();
        $this->form_validation->set_rules($form);

        if ($this->form_validation->run())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function validasi_edit()
    {
        $form = $this->load_form_rules_edit();
        $this->form_validation->set_rules($form);

        if ($this->form_validation->run())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function cari_semua()
    {
        return $this->db->order_by('idmSesi', 'ASC')
                        ->get($this->db_tabel)
                        ->result();
    }

    public function cari($idmSesi)
    {
        return $this->db->where('idmSesi', $idmSesi)
                        ->limit(1)
                        ->get($this->db_tabel)
                        ->row();
    }

    public function buat_tabel($data)
    {
        $this->load->library('table');

        $tmpl = array('row_alt_start'  => '<tr class="bg-yellow">');
        $this->table->set_template($tmpl);

        /// heading tabel
		$this->table->set_heading('ID', 'Jam Mulai', 'Jam Berakhir','Aksi');

        foreach ($data as $row)
        {
            $this->table->add_row(
                $row->idmSesi,
                $row->sesi_mulai,
				$row->sesi_selesai,
                anchor('master_sesi/edit/'.$row->idmSesi,'Edit',array('class' => 'edit')).' '.
                anchor('master_sesi/hapus/'.$row->idmSesi,'Hapus',array('class' => 'delete','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')"))
            );
        }
        $tabel = $this->table->generate();

        return $tabel;
    }

    public function tambah()
    {
        $sesi = array(
                      'sesi_mulai' => $this->input->post('sesi_mulai'),
                      'sesi_selesai' => $this->input->post('sesi_selesai')
                      );
        $this->db->insert($this->db_tabel, $sesi);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function edit($idmSesi)
    {
        $sesi = array(
            'sesi_mulai'=>$this->input->post('sesi_mulai'),
            'sesi_selesai'=>$this->input->post('sesi_selesai'),
        );

        // update db
        $this->db->where('idmSesi', $idmSesi);
		$this->db->update($this->db_tabel, $sesi);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function hapus($idmSesi)
    {
        $this->db->where('idmSesi', $idmSesi)->delete($this->db_tabel);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}
/* End of file model_MasterSesi.php */
/* Location: ./application/models/model_MasterSesi.php */