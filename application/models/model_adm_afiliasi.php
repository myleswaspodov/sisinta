<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_adm_afiliasi extends CI_model
{
	public $db_tabel 	= 'tafiliasi';
	public $per_halaman = 10;
	public $offset 		= 0;
	
	public function load_form_rules_tambah()
	{
		$tambah = array(
			array(
				'label' => 'Instansi Afiliasi',
				'field' => 'idmAfiliasi',
				'rules' => 'required'
			),
			array(
				'label' => 'Nama Mahasiswa',
				'field' => 'idmSkripsi',
				'rules' => "required|is_unique[$this->db_tabel.idmSkripsi]"
			)
		);
		return $tambah;
	}
	
	public function load_form_rules_edit()
	{
		$edit = array(
			array(
				'label' => 'Instansi Afiliasi',
				'field' => 'idmAfiliasi',
				'rules' => 'required'
			),
			array(
				'label' => 'Nama Mahasiswa',
				'field' => 'idmSkripsi',
				'rules' => 'required'
			)
		);
		return $edit;
	}
	
	public function validasi_tambah()
	{
		$form = $this->load_form_rules_tambah();
		$this->form_validation->set_rules($form);
		
		if ($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
 	public function validasi_edit()
    {
        $form = $this->load_form_rules_edit();
        $this->form_validation->set_rules($form);

        if ($this->form_validation->run())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
	
	public function dd_mhs()
	{
		return $this->db->select('skripsi.idmSkripsi,
								mmhsw.mhsw_nama
								')
						->from ('mmhsw,
								skripsi')
						->where ('skripsi.idmMhsw = mmhsw.idmmhsw')
						->get()
						->result();
	}
	
	public function cari_semua($offset)
	{
		if(is_null($offset) || empty($offset))
		{
			$this->offset = 0;
		}
		else
		{
			$this->offset = ($offset * $this->per_halaman) - $this->per_halaman;
		}
		
		return $this->db->select('tafiliasi.idtAfiliasi,
								mafiliasi.mAfiliasi_nama,
								mmhsw.mhsw_nama')
						->from('mmhsw,mafiliasi,skripsi,tafiliasi')
						->where('tafiliasi.idmAfiliasi = mafiliasi.idmAfiliasi')
						->where('skripsi.idmMhsw = mmhsw.idmmhsw')
						->where('tafiliasi.idmSkripsi = skripsi.idmSkripsi')
						->limit($this->per_halaman,$this->offset)
						->order_by('tafiliasi.idtAfiliasi')
						->get()
						->result();
	}
	
	public function buat_tabel($data)
	{
		$this->load->library('table');
		$this->table->set_heading('No','Instansi Afiliasi','Nama Mahasiswa');
		$no = 0 + $this->offset;
		
		foreach($data as $row)
		{
			$this->table->add_row(
			++$no,
			$row->mAfiliasi_nama,
			$row->mhsw_nama,
			anchor('adm_afiliasi/edit/'.$row->idtAfiliasi,'edit',array('class' => 'edit')).' '.
			anchor('adm_afiliasi/hapus/'.$row->idtAfiliasi,'hapus',array('class' => 'delete','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')"))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function cari($idtAfiliasi)
	{
		return $this->db->where('idtAfiliasi',$idtAfiliasi)
						->limit(1)
						->get($this->db_tabel)
						->row();
	}
	
	public function paging($base_url)
	{
		$this->load->library('pagination');
		$config = array (
			'base_url'         => $base_url,
            'total_rows'       => $this->hitung_semua(),
            'per_page'         => $this->per_halaman,
            'num_links'        => 4,
            'use_page_numbers' => TRUE,
            'first_link'       => '&#124;&lt; First',
            'last_link'        => 'Last &gt;&#124;',
            'next_link'        => 'Next &gt;',
            'prev_link'        => '&lt; Prev',
		);
		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}
	
	public function hitung_semua()
	{
		return $this->db->select('tafiliasi.idtAfiliasi,
								mafiliasi.mAfiliasi_nama,
								mmhsw.mhsw_nama')
						->from('mmhsw,mafiliasi,skripsi,tafiliasi')
						->where('tafiliasi.idmAfiliasi = mafiliasi.idmAfiliasi')
						->where('skripsi.idmMhsw = mmhsw.idmmhsw')
						->where('tafiliasi.idmSkripsi = skripsi.idmSkripsi')
						->limit($this->per_halaman,$this->offset)
						->order_by('tafiliasi.idtAfiliasi')
						->get()
						->num_rows();
	}
	
	public function tambah()
	{
		$tambah = array(
			'idmAfiliasi'	=> $this->input->post('idmAfiliasi'),
			'idmSkripsi' 	=> $this->input->post('idmSkripsi')
		);
		$this->db->insert($this->db_tabel, $tambah);
		
		if ($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function edit($idtAfiliasi)
	{
		$edit = array(
		'idmAfiliasi'	=> $this->input->post('idmAfiliasi'),
		'idmSkripsi' 	=> $this->input->post('idmSkripsi')
		);
		$this->db->where('idtAfiliasi',$idtAfiliasi);
		$this->db->update($this->db_tabel, $edit);
		
		if ($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function hapus($idtAfiliasi)
	{
		$this->db->where('idtAfiliasi',$idtAfiliasi)->delete($this->db_tabel);
		
		if ($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
/* End of file model_adm_afiliasi.php */
/* Location: ./application/models/model_adm_afiliasi.php */