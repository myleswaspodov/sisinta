<?php if(!defined('BASEPATH')) exit ('No direct script acces allowed');

class Model_upload_sidang extends CI_Model
{
	public $db_tabel = 'kumpulsyarat';
	public $db_mhs	= 'mmhsw';
	public $db_skrip	= 'skripsi';
	
	public function id_mhs()
	{
		$mhs = $this->session->userdata('users_name');
		return $this->db->select('idmmhsw')
						->from($this->db_mhs)
						->where('mhsw_nim',$mhs)
						->limit(1)
						->get()
						->row()->idmmhsw;
	}
	
	public function get_idS($a)
	{
		return $this->db->select('idmSkripsi')
						->from($this->db_skrip)
						->where('idmMhsw',$a)
						->get()
						->row()->idmSkripsi;
	}
	
	public function untuk_mhs_barumasuk()
	{
		$idMHS = $this->id_mhs();
		$id = $this->get_idS($idMHS);
		return $this->db->select('kumpulsyarat.idKumpulSyarat,mdoksyarat.doksyarat_nama,kumpulsyarat.berkas_unggah,kumpulsyarat.tgl_kumpul,kumpulsyarat.KumpulSyarat_stat,kumpulsyarat.tgl_verif,kumpulsyarat.keterangan')
						->from('kumpulsyarat,mdoksyarat')
						->where('kumpulsyarat.idDokSyarat = mdoksyarat.idmDokSyarat')
						->where('kumpulsyarat.idDokSyarat = 2')
						->where('kumpulsyarat.idmSkripsi',$id)
						->where('lockedit = 0')
						->get()
						->result();
	}
	
	public function untuk_mhs_diterima()
	{
		$idMHS = $this->id_mhs();
		$id = $this->get_idS($idMHS);
		return $this->db->select('kumpulsyarat.idKumpulSyarat,mdoksyarat.doksyarat_nama,kumpulsyarat.berkas_unggah,kumpulsyarat.tgl_kumpul,kumpulsyarat.KumpulSyarat_stat,kumpulsyarat.tgl_verif,kumpulsyarat.keterangan')
						->from('kumpulsyarat,mdoksyarat')
						->where('kumpulsyarat.idDokSyarat = mdoksyarat.idmDokSyarat')
						->where('kumpulsyarat.idDokSyarat = 2')
						->where('kumpulsyarat.idmSkripsi',$id)
						->where('lockedit = 2')
						->get()
						->result();
	}
	
	public function untuk_mhs_didonlod()
	{
		$idMHS = $this->id_mhs();
		$id = $this->get_idS($idMHS);
		return $this->db->select('kumpulsyarat.idKumpulSyarat,mdoksyarat.doksyarat_nama,kumpulsyarat.berkas_unggah,kumpulsyarat.tgl_kumpul,kumpulsyarat.KumpulSyarat_stat,kumpulsyarat.tgl_verif,kumpulsyarat.keterangan')
						->from('kumpulsyarat,mdoksyarat')
						->where('kumpulsyarat.idDokSyarat = mdoksyarat.idmDokSyarat')
						->where('kumpulsyarat.idDokSyarat = 2')
						->where('kumpulsyarat.idmSkripsi',$id)
						->where('lockedit = 1')
						->get()
						->result();
	}
	
	public function untuk_mhs_ditolak()
	{
		$idMHS = $this->id_mhs();
		$id = $this->get_idS($idMHS);
		return $this->db->select('kumpulsyarat.idKumpulSyarat,mdoksyarat.doksyarat_nama,kumpulsyarat.berkas_unggah,kumpulsyarat.tgl_kumpul,kumpulsyarat.KumpulSyarat_stat,kumpulsyarat.tgl_verif,kumpulsyarat.keterangan')
						->from('kumpulsyarat,mdoksyarat')
						->where('kumpulsyarat.idDokSyarat = mdoksyarat.idmDokSyarat')
						->where('kumpulsyarat.idDokSyarat = 2')
						->where('kumpulsyarat.idmSkripsi',$id)
						->where('lockedit = 3')
						->get()
						->result();
	}
	
	
	public function sudah_diver()
	{
		return $this->db->select('kumpulsyarat.idKumpulSyarat,mmhsw.mhsw_nama,mdoksyarat.doksyarat_nama,kumpulsyarat.berkas_unggah,kumpulsyarat.tgl_kumpul,kumpulsyarat.KumpulSyarat_stat,kumpulsyarat.tgl_verif')
						->from('mmhsw,skripsi,kumpulsyarat,mdoksyarat')
						->where('kumpulsyarat.idDokSyarat = mdoksyarat.idmDokSyarat')
						->where('kumpulsyarat.idmSkripsi = skripsi.idmSkripsi')
						->where('skripsi.idmMhsw = mmhsw.idmmhsw')
						->where('kumpulsyarat.idDokSyarat = 2')
						->where('lockedit = 2')
						->get()
						->result();
	}
	
	public function sudah_didonload()
	{
		return $this->db->select('kumpulsyarat.idKumpulSyarat,mmhsw.mhsw_nama,mdoksyarat.doksyarat_nama,kumpulsyarat.berkas_unggah,kumpulsyarat.tgl_kumpul,kumpulsyarat.KumpulSyarat_stat,kumpulsyarat.tgl_verif')
						->from('mmhsw,skripsi,kumpulsyarat,mdoksyarat')
						->where('kumpulsyarat.idDokSyarat = mdoksyarat.idmDokSyarat')
						->where('kumpulsyarat.idmSkripsi = skripsi.idmSkripsi')
						->where('kumpulsyarat.idDokSyarat = 2')
						->where('skripsi.idmMhsw = mmhsw.idmmhsw')
						->where('lockedit = 1')
						->get()
						->result();
	}
	
	public function baru_masuk()
	{
		return $this->db->select('kumpulsyarat.idKumpulSyarat,mmhsw.mhsw_nama,mdoksyarat.doksyarat_nama,kumpulsyarat.berkas_unggah,kumpulsyarat.tgl_kumpul,kumpulsyarat.KumpulSyarat_stat,kumpulsyarat.tgl_verif')
						->from('mmhsw,skripsi,kumpulsyarat,mdoksyarat')
						->where('kumpulsyarat.idDokSyarat = mdoksyarat.idmDokSyarat')
						->where('kumpulsyarat.idmSkripsi = skripsi.idmSkripsi')
						->where('kumpulsyarat.idDokSyarat = 2')
						->where('skripsi.idmMhsw = mmhsw.idmmhsw')
						->where('lockedit = 0')
						->get()
						->result();
	}
	
	public function tabel_mhs($data)
	{
		$this->load->library('table');
		
		$this->table->set_heading('Dokumen Syarat','Berkas','Tanggal Upload','Status');
		foreach($data as $row)
		{
			$hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->tgl_kumpul));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->tgl_kumpul));
            $hr_tgl = "$hari, $tgl";
			
			$this->table->add_row(
				$row->doksyarat_nama,
				$row->berkas_unggah,
				$hr_tgl,
				$row->KumpulSyarat_stat
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function tabel_mhs_diterima($data)
	{
		$this->load->library('table');
		
		$this->table->set_heading('Dokumen Syarat','Berkas','Tanggal Upload','Status','Tanggal Verifikasi');
		foreach($data as $row)
		{
			$hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->tgl_kumpul));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->tgl_kumpul));
            $hr_tgl = "$hari, $tgl";
			
			$wayah = date('w', strtotime($row->tgl_verif));
			$dino = $hari_array[$wayah];
			$tanggal = date('d-m-Y', strtotime($row->tgl_verif));
			$hrtglv = "$dino, $tanggal ";
			
			$this->table->add_row(
				$row->doksyarat_nama,
				$row->berkas_unggah,
				$hr_tgl,
				$row->KumpulSyarat_stat,
				$hrtglv
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	
	public function tabel_mhs_ditolak($data)
	{
		$this->load->library('table');
		
		$this->table->set_heading('Dokumen Syarat','Berkas','Tanggal Upload','Status','Keterangan');
		foreach($data as $row)
		{
			$hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->tgl_kumpul));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->tgl_kumpul));
            $hr_tgl = "$hari, $tgl";
			
			$wayah = date('w', strtotime($row->tgl_verif));
			$dino = $hari_array[$wayah];
			$tanggal = date('d-m-Y', strtotime($row->tgl_verif));
			$hrtglv = "$dino, $tanggal ";
			
			$this->table->add_row(
				$row->doksyarat_nama,
				$row->berkas_unggah,
				$hr_tgl,
				$row->KumpulSyarat_stat,
				$row->keterangan,
				anchor('upload_berkas_sidang/upload_lagi/'.$row->idKumpulSyarat,'Upload Ulang',array('class'=>'upload'))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function tabel_adm($data)
	{
		$this->load->library('table');
		
		$this->table->set_heading('Dokumen Syarat','Nama','Berkas','Tanggal Upload','Status');
		foreach($data as $row)
		{
			$hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->tgl_kumpul));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->tgl_kumpul));
            $hr_tgl = "$hari, $tgl";
			
			$this->table->add_row(
				$row->doksyarat_nama,
				$row->mhsw_nama,
				$row->berkas_unggah,
				$hr_tgl,
				$row->KumpulSyarat_stat,
				anchor('koor_download_sidang/download/'.$row->berkas_unggah,'Download',array('class' => 'download'))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function tabel_adm_ygvalid($data)
	{
		$this->load->library('table');
		
		$this->table->set_heading('Dokumen Syarat','Nama','Berkas','Tanggal Upload','Status','Tanggal Verifikasi');
		foreach($data as $row)
		{
			$hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->tgl_kumpul));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->tgl_kumpul));
            $hr_tgl = "$hari, $tgl";
			
			$wayah = date('w', strtotime($row->tgl_verif));
			$dino = $hari_array[$wayah];
			$tanggal = date('d-m-Y', strtotime($row->tgl_verif));
			$hrtglv = "$dino, $tanggal ";
			
			$this->table->add_row(
				$row->doksyarat_nama,
				$row->mhsw_nama,
				$row->berkas_unggah,
				$hr_tgl,
				$row->KumpulSyarat_stat,
				$hrtglv
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function tabelsudahdonlod_adm($data)
	{
		$this->load->library('table');
		
		$this->table->set_heading('Dokumen Syarat','Nama','Berkas','Tanggal Upload','Status','aksi');
		foreach($data as $row)
		{
			$hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->tgl_kumpul));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->tgl_kumpul));
            $hr_tgl = "$hari, $tgl";
			
			$this->table->add_row(
				$row->doksyarat_nama,
				$row->mhsw_nama,
				$row->berkas_unggah,
				$hr_tgl,
				$row->KumpulSyarat_stat,
			
				anchor('koor_download_sidang/verifikasi/'.$row->idKumpulSyarat,'Terima',array('class' => 'oyi')).'<br><br> '.
				anchor('koor_download_sidang/ditolak/'.$row->idKumpulSyarat,'Tolak',array('class' => 'tolak'))
				
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function upload_berkas($unggah)
	{
		$a= $this->get_idS($this->id_mhs());
		$upload = array(
			'idmSkripsi' => $a,
			'idDokSyarat' => 2,
			'tgl_kumpul' => date('Y-m-d H:m:s'),
			'berkas_unggah' =>  $unggah,
			'lockedit'	=> 0,
			'KumpulSyarat_stat' => 'Menunggu Konfirmasi',
		);
		$this->db->insert($this->db_tabel,$upload);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function tandai_sudahdonlod($id)
	{
		$tandai = array(
			'lockedit' => 1
		);
		$this->db->where('berkas_unggah',$id)->update($this->db_tabel,$tandai);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function diterima($id)
	{
		$oke = array(
			'lockedit' => 2,
			'tgl_verif' => date('Y-m-d'),
			'KumpulSyarat_stat' => 'Syarat sidang valid',
		);
		$this->db->where('idKumpulSyarat',$id)->update($this->db_tabel,$oke);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function ket()
	{
		$form = array(
			'field' => 'keterangan',
			'label' => 'Keterangan',
			'rules' => 'required',
		);
		return $form;
	}
	
	public function validasi_ket()
	{
		$form = $this->ket();
		$this->form_validation->set_rules($form);
		
		if($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function ditolak($id)
	{
		$tolak = array(
			'lockedit' => 3,
			'keterangan' => $this->input->post('keterangan'),
			'KumpulSyarat_stat' => 'Syarat tidak memenuhi',
		);
		$this->db->where('idKumpulSyarat',$id)->update($this->db_tabel,$tolak);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function get_skripsi($id)
	{
		return $this->db->select('idmSkripsi')
						->from($this->db_tabel)
						->where('idKumpulSyarat',$id)
						->get()
						->row()->idmSkripsi;
	}
	
	public function ubah_status_skripsi($id)
	{
		$oke = array(
			'idmStatusPros' => 5
		);
		$this->db->where('idmSkripsi',$id)->update($this->db_skrip,$oke);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function hapusdulu($id)
	{
		$this->db->where('idKumpulSyarat',$id)->delete($this->db_tabel);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
}