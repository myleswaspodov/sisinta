<?php if (!defined('BASEPATH')) exit('No direct script allowed');

class Model_admsediaruangkhusus extends CI_Model
{
	public $db_tabel	= 'sediaruangtanggal';
	public $per_halaman	= 10;
	public $offset		= 0;
	
	public function load_form_rules_tambah()
	{
		$form = array(
					array(
						'label' => 'Tanggal',
						'field'	=> 'srt_tgl',
						'rules'	=> 'required|callback_is_format_tanggal'
					),
					array(
						'label'	=> 'Ruang',
						'field'	=> 'idmRuang',
						'rules'	=> 'required'
					),
					array(
						'label'	=> 'Jam Mulai',
						'field'	=> 'idmSesi_awal',
						'rules' => 'required'
					),
					array(
						'label'	=> 'Jam Mulai',
						'field'	=> 'idmSesi_akhir',
						'rules' => 'required'
					)
		);
		return $form;
	}
	
	public function load_form_rules_edit()
	{
		$form = array(
					array(
						'label' => 'Tanggal',
						'field'	=> 'srt_tgl',
						'rules'	=> 'required|callback_is_format_tanggal'
					),
					array(
						'label'	=> 'Ruang',
						'field'	=> 'idmRuang',
						'rules'	=> 'required'
					),
					array(
						'label'	=> 'Jam Mulai',
						'field'	=> 'idmSesi_awal',
						'rules' => 'required'
					),
					array(
						'label'	=> 'Jam Mulai',
						'field'	=> 'idmSesi_akhir',
						'rules' => 'required'
					)
		);
		return $form;
	}
	
	public function validasi_tambah()
	{
		$form = $this->load_form_rules_tambah();
		$this->form_validation->set_rules($form);
		
		if($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function validasi_edit()
	{
		$form = $this->load_form_rules_edit();
		$this->form_validation->set_rules($form);
		
		if($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function cari_semua($offset)
	{
		if(is_null($offset)||empty($offset))
		{
			$this->offset = 0;
		}
		else
		{
			$this->offset = ($offset * $this->per_halaman) - $this->per_halaman;
		}
		
		return $this->db->select('mruang.ruang_nama,a.sesi_mulai,b.sesi_selesai,idSediaRuangTanggal,srt_tgl')
						->from('sediaruangtanggal, msesi a, msesi b, mruang')
						->where('sediaruangtanggal.idmRuang = mruang.idmRuang')
						->where('sediaruangtanggal.idmSesi_awal = a.idmSesi')
						->where('sediaruangtanggal.idmSesi_akhir = b.idmSesi')
						->limit($this->per_halaman, $this->offset)
						->order_by('srt_tgl','dsc')
						->get()
						->result();
	}
	
	public function cari($idSediaRuangTanggal)
	{
		return $this->db->where('idSediaRuangTanggal', $idSediaRuangTanggal)
						->limit(1)
						->get($this->db_tabel)
						->row();
	}
	
	public function buat_tabel($data)
	{
		$this->load->library('table');
		$this->table->set_heading('Tanggal','Ruang','Jam Mulai','Jam Selesai');
		
		foreach($data as $row)
		{
			
			// Konversi hari dan tanggal ke dalam format Indonesia (dd-mm-yyyy)
            $hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->srt_tgl));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->srt_tgl));
            $hr_tgl = "$hari, $tgl";
		
			$this->table->add_row(
				$hr_tgl,
				$row->ruang_nama,
				$row->sesi_mulai,
				$row->sesi_selesai,
				anchor('adm_sediaruangkhusus/edit/'.$row->idSediaRuangTanggal,'Edit',array('class' => 'edit')).' '.
                anchor('adm_sediaruangkhusus/hapus/'.$row->idSediaRuangTanggal,'Hapus',array('class'=> 'delete','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')"))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function paging($base_url)
	{
		$this->load->library('pagination');
		$config = array(
			'base_url'         => $base_url,
            'total_rows'       => $this->hitung_semua(),
            'per_page'         => $this->per_halaman,
            'num_links'        => 2,			
			'use_page_numbers' => TRUE,
            'first_link'       => '&#124;&lt; First',
            'last_link'        => 'Last &gt;&#124;',
            'next_link'        => 'Next &gt;',
            'prev_link'        => '&lt; Prev',
		);
		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}
	
	public function hitung_semua()
	{
		return $this->db->count_all($this->db_tabel);
	}
	
	public function tambah()
	{
		$tambah = array(
			'srt_tgl' => date('Y-m-d',strtotime($this->input->post('srt_tgl'))),
			'idmRuang' => $this->input->post('idmRuang'),
			'idmSesi_awal' => $this->input->post('idmSesi_awal'),
			'idmSesi_akhir' => $this->input->post('idmSesi_akhir')
		);
		$this->db->insert($this->db_tabel, $tambah);
		
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function edit($idSediaRuangTanggal)
	{
		$edit = array(
			'srt_tgl' => date('Y-m-d', strtotime($this->input->post('srt_tgl'))),
			'idmRuang' => $this->input->post('idmRuang'),
			'idmSesi_awal' => $this->input->post('idmSesi_awal'),
			'idmSesi_akhir' => $this->input->post('idmSesi_akhir')
		);
		$this->db->where('idSediaRuangTanggal', $idSediaRuangTanggal)->update($this->db_tabel, $edit);
		
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function hapus($idSediaRuangTanggal)
	{
		$this->db->where('idSediaRuangTanggal', $idSediaRuangTanggal)->delete($this->db_tabel);
		
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
}

/* End of file model_admsediaruangkhusus.php */
/* Location: ./application/models/model_admsediaruangkhusus.php */