<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_masterstatusskripsi extends CI_Model {

    public $db_tabel    = 'mstatusskrip';
    
	public function __construct()
	{
		parent::__construct();
	}

    // rules form validasi, proses TAMBAH
    private function load_form_rules_tambah()
    {
        $form = array(
                        
						array(
                            'field' => 'idmStatusSkrip',
                            'label' => 'ID',
                            'rules' => "required|numeric|is_unique[$this->db_tabel.idmStatusSkrip]"
                        ),
                        array(
                            'field' => 'StatusSkrip_tipe',
                            'label' => 'Status Skripsi',
                            'rules' => 'required|max_length[50]'
                        ),
        );
        return $form;
    }

    // rules form validasi, proses EDIT
    private function load_form_rules_edit()
    {
        $form = array(
                        
						array(
                            'field' => 'idmStatusSkrip',
                            'label' => 'ID',
                            'rules' => 'required|numeric'
                        ),
                        array(
                            'field' => 'StatusSkrip_tipe',
                            'label' => 'Status Skripsi',
                            'rules' => 'required|max_length[50]'
                        ),
        );
        return $form;
    }

    // jalankan proses validasi, untuk operasi TAMBAH
    public function validasi_tambah()
    {
        $form = $this->load_form_rules_tambah();
        $this->form_validation->set_rules($form);

        if ($this->form_validation->run())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    // jalankan proses validasi, untuk operasi EDIT
    public function validasi_edit()
    {
        $form = $this->load_form_rules_edit();
        $this->form_validation->set_rules($form);

        if ($this->form_validation->run())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function cari_semua()
	{
       return $this->db->order_by('idmStatusSkrip','ASC')
						->get($this->db_tabel)
						->result();
	}

    public function cari($idmStatusSkrip)
    {
        return $this->db->where('idmStatusSkrip', $idmStatusSkrip)
            ->limit(1)
            ->get($this->db_tabel)
            ->row();
    }

    public function buat_tabel($data)
    {
        $this->load->library('table');

        // buat class zebra di <tr>,untuk warna selang-seling
        $tmpl = array('row_alt_start'  => '<tr class="bg-yellow">');
        $this->table->set_template($tmpl);

        // heading tabel
        $this->table->set_heading('ID', 'Status Skripsi', 'Aksi');

        foreach ($data as $row)
        {
            $this->table->add_row(
                $row->idmStatusSkrip,
                $row->StatusSkrip_tipe,
                anchor('master_statusskripsi/edit/'.$row->idmStatusSkrip,'Edit',array('class' => 'edit')).' '.
                anchor('master_statusskripsi/hapus/'.$row->idmStatusSkrip,'Hapus',array('class' => 'delete','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')"))
            );
        }
        $tabel = $this->table->generate();

        return $tabel;
    }

    public function tambah()
    {
        $SK = array(
			'idmStatusSkrip' => $this->input->post('idmStatusSkrip'),
            'StatusSkrip_tipe' => $this->input->post('StatusSkrip_tipe'),
        );
        $this->db->insert($this->db_tabel, $SK);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function edit($idmStatusSkrip)
    {
        $SK = array(
            'idmStatusSkrip'=>$this->input->post('idmStatusSkrip'),
            'StatusSkrip_tipe'=>$this->input->post('StatusSkrip_tipe'),
        );

        // update db
        $this->db->where('idmStatusSkrip', $idmStatusSkrip);
        $this->db->update($this->db_tabel, $SK);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function hapus($idmStatusSkrip)
    {
        $this->db->where('idmStatusSkrip', $idmStatusSkrip)->delete($this->db_tabel);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}
/* End of file model_MasterStatusSkripsi.php */
/* Location: ./application/models/model_MasterMasterStatusSkripsi.php */