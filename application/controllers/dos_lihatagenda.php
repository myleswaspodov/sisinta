<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dos_lihatagenda extends MY_Controller
{
	public $data = array(
			'breadcrumb'	=> 'Agenda',
			'main_view'		=> 'dos_agendadosen/agendadosen_dos',
			'pesan'			=> '',
			'pagination'	=> '',
			'tabel_data'	=> '',
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_dos_lihatagenda','agenda',TRUE);
	}
	
	public function index($offset = 0)
	{
		$agenda = $this->agenda->cari_semua($offset);
		if($agenda)
		{
			$tabel = $this->agenda->buat_tabel($agenda);
			$this->data['tabel_data'] = $tabel;
			$this->data['pagination'] = $this->agenda->paging(site_url('agenda/halaman'));
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada agenda untuk anda';
		}
		$this->load->view('template_dos',$this->data);
	}
	
}