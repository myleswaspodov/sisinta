<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Master_ruang extends MY_Controller {

    public $data = array(
                        'breadcrumb'    => 'Master Ruangan',
                        'pesan'         => '',
                        'pagination'    => '',
                        'tabel_data'    => '',
                        'main_view'     => 'm_ruang/ruang',
                        'form_action'   => '',
                        'form_value'    => '',
                        );

    public function __construct()
	{
		parent::__construct();		
		$this->load->model('model_masterruang', 'ruang', TRUE);
    }

	public function index($offset = 0)
	{
        // hapus data temporary proses update
        $this->session->unset_userdata('id_sekarang', '');
        $this->session->unset_userdata('ruang_sekarang', '');

        // Cari semua data kelas
        $ruang = $this->ruang->cari_semua($offset);
		
		// data kelas ada, tampilkan
        if ($ruang)
        {
            // buat tabel
            $tabel = $this->ruang->buat_tabel($ruang);
            $this->data['tabel_data'] = $tabel;
			$this->data['pagination'] = $this->ruang->paging(site_url('ruangan/halaman'));
            $this->load->view('template_adm', $this->data);
        }
        // data kelas tidak ada
        else
        {
            $this->data['pesan'] = 'Tidak ada data ruang.';
            $this->load->view('template_adm', $this->data);
        }
	}

    public function tambah()
    {
        $this->data['breadcrumb']  = 'Ruang > Tambah';
        $this->data['main_view']   = 'm_ruang/ruang_form';
        $this->data['form_action'] = 'master_ruang/tambah';

        // submit
        if($this->input->post('submit'))
        {
            // validasi sukses
            if($this->ruang->validasi_tambah())
            {
                if($this->ruang->tambah())
                {
                    $this->session->set_flashdata('pesan', 'Proses tambah data berhasil.');
                    redirect('master_ruang');
                }
                else
                {
                    $this->data['pesan'] = 'Proses tambah data gagal.';
                    $this->load->view('template_adm', $this->data);
                }
            }
            // validasi gagal
            else
            {
                $this->load->view('template_adm', $this->data);
            }
        }
        // no submit
        else
        {
            $this->load->view('template_adm', $this->data);
        }
    }

    public function edit($idmRuang = NULL)
    {
        $this->data['breadcrumb']  = 'Ruang > Edit';
        $this->data['main_view']   = 'm_ruang/ruang_form';
        $this->data['form_action'] = 'master_ruang/edit/' . $idmRuang;

        // pastikan id_kelas ada
        if( ! empty($idmRuang))
        {
            // submit
            if($this->input->post('submit'))
            {
                // validasi berhasil
                if($this->ruang->validasi_edit() === TRUE)
                {
                    //update db
                    $this->ruang->edit($this->session->userdata('id_sekarang'));
                    $this->session->set_flashdata('pesan', 'Proses update data berhasil.');

                    redirect('master_ruang');
                }
                // validasi gagal
                else
                {
                    $this->load->view('template_adm', $this->data);
                }
            }
            // tidak disubmit, form pertama kali dimuat
            else
            {
                // ambil data dari database, $form_value sebagai nilai dafault form
                $ruang = $this->ruang->cari($idmRuang);
                foreach($ruang as $key => $value)
                {
                    $this->data['form_value'][$key] = $value;
                }

                // set temporary data for edit
                $this->session->set_userdata('id_sekarang', $ruang->idmRuang);
                $this->session->set_userdata('ruang_sekarang', $ruang->ruang_nama);

                $this->load->view('template_adm', $this->data);
            }
        }
        // tidak ada parameter id_kelas, kembalikan ke halaman kelas
        else
        {
            redirect('master_ruang');
        }
    }

    public function hapus($idmRuang = NULL)
    {
        // pastikan id_kelas yang akan dihapus
        if( ! empty($idmRuang))
        {
            if($this->ruang->hapus($idmRuang))
            {
                $this->session->set_flashdata('pesan', 'Proses hapus data berhasil.');
                redirect('master_ruang');
            }
            else
            {
                $this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
                redirect('master_ruang');
            }
        }
        else
        {
            $this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
            redirect('master_ruang');
        }
    }

    // callback, apakah id_kelas sama? untuk proses edit
    function is_id_ruang_exist()
    {
        $id_sekarang 	= $this->session->userdata('id_sekarang');
        $id_baru		= $this->input->post('idmRuang');

        // jika id_kelas baru dan id_kelas yang sedang diedit sama biarkan
        // artinya id_kelas tidak diganti
        if ($id_baru === $id_sekarang)
        {
            return TRUE;
        }
        // jika id_kelas yang sedang diupdate (di session) dan yang baru (dari form) tidak sama,
        // artinya id_kelas mau diganti
        // cek di database apakah id_kelas sudah terpakai?
        else
        {
            // cek database untuk id_kelas yang sama
            $query = $this->db->get_where('mruang', array('idmRuang' => $id_baru));

            // id_kelas sudah dipakai
            if($query->num_rows() > 0)
            {
                $this->form_validation->set_message('is_id_ruang_exist',
                                                    "Ruangan dengan kode $id_baru sudah terdaftar");
                return FALSE;
            }
            // id_kelas belum dipakai, OK
            else
            {
                return TRUE;
            }
        }
    }

    // callback, apakah nama kelas sama? untuk proses edit
    // penjelasan kurang lebih sama dengan is_id_kelas_exist
    function is_ruang_exist()
    {
        $ruang_sekarang 	= $this->session->userdata('ruang_sekarang');
        $ruang_baru		= $this->input->post('ruang_nama');

        if ($ruang_baru === $ruang_sekarang)
        {
            return TRUE;
        }
        else
        {
            // cek database untuk nama kelas yang sama
            $query = $this->db->get_where('mruang', array('ruang_nama' => $ruang_baru));
            if($query->num_rows() > 0)
            {
                $this->form_validation->set_message('is_ruang_exist',
                                                    "Ruangan dengan nama $ruang_baru sudah terdaftar");
                return FALSE;
            }
            else
            {
                return TRUE;
            }
        }
    }
}
/* End of file master_ruang.php */
/* Location: ./application/controllers/master_ruang.php */