<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Adm_lupapasword extends MY_Controller
{
	public $data = array(
		'breadcrumb' => 'Manajemen Lupa Password dan Hapus Pengguna',
		'pesan' => '',
		'tabel_data' => '',
		'main_view' => 'adm_hakakses/adm_lupapasword',
		'form_action' => 'search_pass/cari',
		
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_adm_lupapasword','hak',TRUE);
	}
	
	public function index($offset = 0)
	{
		$semua = $this->hak->semua($offset);
		if($semua)
		{
			$this->data['tabel_data'] = $this->hak->tabel_lupapass($semua);
			$this->data['pagination'] = $this->hak->paging(site_url('lupapass/halaman'));
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada user';
		}
		$this->load->view('template_adm',$this->data);
	}
	
	public function normal($users_name,$user)
	{
		$lupa = $this->hak->passbaru($users_name,$user);
		if($lupa)
		{
			$this->session->set_flashdata('pesan','Password telah diperbaiki! Password sama dengan username!');
			redirect('adm_lupapasword');
		}
		else
		{
			$this->session->set_flashdata('pesan','Password sudah diperbaiki!!');
			redirect('adm_lupapasword');
			//$this->load->view('template_adm',$this->data);
		}
		$this->load->view('template_adm',$this->data);
	}
	
	public function hapus($id)
    {
        if( ! empty($id))
        {
            if($this->hak->hapus_adm($id))
            {
                $this->session->set_flashdata('pesan', 'Proses hapus user berhasil.');
                redirect('adm_lupapasword');
            }
            else
            {
                $this->session->set_flashdata('pesan', 'Proses hapus user gagal.');
                redirect('adm_lupapasword');
            }
        }
        else
        {
            $this->session->set_flashdata('pesan', 'Proses hapus user gagal.');
            redirect('adm_lupapasword');
        }
    }
	
}