<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class koor_download_ygmana extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$data['breadcrumb'] = "Periksa Syarat";
		$data['main_view'] = 'download/donlodyangmana';
		
		$this->load->view("template_koor",$data);
	}
}