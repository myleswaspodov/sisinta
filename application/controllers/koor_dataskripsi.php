<?php if(!defined('BASEPATH')) exit ('No direct script acces allowed');

class Koor_dataskripsi extends MY_Controller
{
	public $data = array (
			'breadcrumb' => 'Registrasi Skripsi',
			'main_view' => 'koor_dataskripsi/dataskripsi',
			'pesan' => '',
			'tabel_data' => '',
			'pagination' => '',
			'form_action' => 'search_dataskripsi/cari',
			'form_value' => '',
			'opsi_mhs' => '',
			'opsi_p1' => '',
			'opsi_p2' => '',
			'opsi_k1' => '',
			'opsi_k2' => '',
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_koor_dataskripsi','skripsi',TRUE);
		$this->load->model('model_masterdosen','dosen',TRUE);
		$this->load->model('model_masterkategori','kateg',TRUE);
		$this->load->model('model_all_katalogskripsi','datanyaskripsi',TRUE);
	}
	
	public function index($offset = 0)
	{
		$this->load->view('template_koor',$this->data);
	}
	
	public function detail($id)
	{
		$data['lihat'] = $this->skripsi->detail($id);
		$this->load->view('koor_dataskripsi/detail',$data);
	}
	
	public function barumasuk($offset = 0)
	{
		$this->data['breadcrumb'] = 'Data Skripsi Belum Terverifikasi';
		$dataskrip = $this->skripsi->skripsi_baru($offset);
		if($dataskrip)
		{
			$tabel = $this->skripsi->buat_tabel($dataskrip);
			$this->data['tabel_data'] = $tabel;
			$this->data['pagination'] = $this->skripsi->paging_baru(site_url('dataskripsibaru/halaman'));
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada skripsi baru. Semua skripsi sudah valid';
		}
		$this->load->view('template_koor',$this->data);
	}
	
	public function yangsudahvalid($offset = 0)
	{
		$this->data['breadcrumb'] = 'Data Skripsi Terverifikasi';
		$dataskrip = $this->skripsi->skripsi_oke($offset);
			if($dataskrip)
			{
				$tabel = $this->skripsi->buat_tabel($dataskrip);
				$this->data['tabel_data'] = $tabel;
				$this->data['pagination'] = $this->skripsi->paging_oke(site_url('dataskripsivalid/halaman'));
			}
			else
			{
				$this->data['pesan'] = "Tidak ada data skripsi";
			}
			$this->load->view('template_koor',$this->data);
	}
	
	public function edit($idmSkripsi = NULL)
	{
		$this->data['main_view'] = 'koor_dataskripsi/dataskripsiedit_form';
		$this->data['form_action'] = 'koor_dataskripsi/edit/'.$idmSkripsi;
		$this->data['breadcrumb'] = 'Edit Data Skripsi';
		
		$dos1 = $this->dosen->dd_dos();
		if($dos1)
		{
			foreach($dos1 as $row)
			{
				$this->data['opsi_p1']['00'] = '-- Pilih Dosen Pembimbing 1 --';
				$this->data['opsi_p1'][$row->idmDosen] = $row->dos_nama;
			}
		}
		else
		{
			$this->data['opsi_p1']['00'] = '-';
			$this->data['pesan'] = 'Data Dosen tidak tersedia';
		}
////////////////////////
		$dos2 = $this->dosen->dd_dos();
		if($dos2)
		{
			foreach($dos2 as $row)
			{
				$this->data['opsi_p2'][''] = '-- Pilih Dosen Pembimbing 2--';
				$this->data['opsi_p2'][$row->idmDosen] = $row->dos_nama;
			}
		}
		else
		{
			$this->data['opsi_p2']['00'] = '-';
			$this->data['pesan'] = 'Data Dosen tidak tersedia';
		}
////////////////////////
		$kat1 = $this->kateg->dd_kat();
		if($kat1)
		{
			foreach($kat1 as $row)
			{
				$this->data['opsi_k1']['00'] = '-- Pilih Kategori --';
				$this->data['opsi_k1'][$row->idmKategori] = $row->katg_nama;
			}
		}
		else
		{
			$this->data['opsi_k1']['00'] = '-';
			$this->data['pesan'] = 'Tidak ada kategori';
		}
//////////////////////
		$kat2 = $this->kateg->dd_kat();
		if($kat2)
		{
			foreach($kat2 as $row)
			{
				$this->data['opsi_k2']['00'] = '-- Pilih Kategori --';
				$this->data['opsi_k2'][$row->idmKategori] = $row->katg_nama;
			}
		}
		else
		{
			$this->data['opsi_k2']['00'] = '-';
			$this->data['pesan'] = 'Tidak ada kategori';
		}

		if(! empty($idmSkripsi))
		{
			if($this->input->post('submit'))
			{
				if($this->skripsi->validasi_edit() === TRUE )
				{
					$this->skripsi->edit($idmSkripsi);
					$this->session->set_flashdata('pesan','Update data skripsi berhasil');
					redirect('koor_dataskripsi');
				}
				else
				{
					$this->load->view('template_koor',$this->data);
				}
			}
			else
			{
				$skrip = $this->skripsi->cari($idmSkripsi);
				foreach($skrip as $key=>$value)
				{
					$this->data['form_value'][$key] = $value;
				}
				$this->session->set_userdata('id_sekarang',$skrip->idmSkripsi);
				$this->load->view('template_koor',$this->data);
			}
		}
		else
		{
			redirect('koor_dataskripsi');
		}
	}
	
	public function hapus($id)
	{
		 if( ! empty($id))
        {
            if($this->skripsi->hapus($id))
            {
                $this->session->set_flashdata('pesan', 'Proses hapus data berhasil.');
                redirect('koor_dataskripsi');
            }
            else
            {
                $this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
                redirect('koor_dataskripsi');
            }
        }
        else
        {
            $this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
            redirect('koor_dataskripsi');
        }
	}
	
	public function diterima($id)
	{
		if($this->skripsi->iya($id))
		{
			$this->session->set_flashdata('pesan','Skripsi sudah terverifikasi');
			redirect('koor_dataskripsi/barumasuk');
		}
		else
		{
			$this->session->set_flashdata('pesan','Status skripsi belum terverifikasi.');
			redirect('koor_dataskripsi/barumasuk');
		}
	}
	
	public function ditolak($id)
	{
		if($this->skripsi->tidak($id))
		{
			$this->session->set_flashdata('pesan','Skripsi ditolak');
			redirect('koor_dataskripsi/barumasuk');
		}
		else
		{
			$this->session->set_flashdata('pesan','Status skripsi masih terverifikasi.');
			redirect('koor_dataskripsi/barumasuk');
		}
	}
	
	 public function is_format_tanggal($str)
    {
        if( ! preg_match('/(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-([0-9]{4})/', $str))
        {
            $this->form_validation->set_message('is_format_tanggal', 'Format tanggal tidak valid. (dd-mm-yyyy)');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
}

/* End of file koor_dataskripsi.php */
/* Location: ./application/controller/koor_dataskripsi.php */