<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Adm_admintandingan extends MY_Controller
{
	public $data = array(
		'breadcrumb' 	=> 'Tambah Admin',
		'pesan'			=> '',
		'tabel_data'	=> '',
		'main_view'		=> 'adm_hakakses/adm_tandingan',
		'form_action'	=> '',
		
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_adm_hakakses','akses',TRUE);
	}
	
	public function index()
	{
		$tampilan = $this->akses->para_admin();
		if($tampilan)
		{
			$this->data['tabel_data'] = $this->akses->tabel_admin($tampilan);
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada Administrasi';
		}
		$this->load->view('template_adm',$this->data);
	}
	
	public function tambah()
	{
		$this->data['main_view'] = 'adm_hakakses/adm_tandingan_form';
		$this->data['form_action'] = 'adm_admintandingan/tambah';
		$this->data['breadcrumb'] = 'Tambah Admin';
		
		if($this->input->post('submit'))
		{
			if($this->akses->validasi_adm())
			{
				if($this->akses->adm_tandingan())
				{
					$this->session->set_flashdata('pesan','Admin baru telah ditambah');
					redirect('adm_admintandingan');
				}
				else
				{
					$this->data['pesan'] = 'Penambahan Admin baru gagal';
					$this->load->view('template_adm',$this->data);
				}
			}
			else
			{
				$this->data['pesan'] = 'Isian tidak boleh kosong!';
				$this->load->view('template_adm',$this->data);
			}
		}
		else
		{
			$this->load->view('template_adm',$this->data);
		}
	}
	
	public function hapus($id)
    {
        if( ! empty($id))
        {
            if($this->akses->hapus_adm($id))
            {
                $this->session->set_flashdata('pesan', 'Proses hapus admin berhasil.');
                redirect('adm_admintandingan');
            }
            else
            {
                $this->session->set_flashdata('pesan', 'Proses hapus admin gagal.');
                redirect('adm_admintandingan');
            }
        }
        else
        {
            $this->session->set_flashdata('pesan', 'Proses hapus data gagal.');
            redirect('adm_admintandingan');
        }
    }
}