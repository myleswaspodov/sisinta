<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Adm_sediaruang extends MY_Controller
{
	public $data = array (
		'breadcrumb'		=> 'Sedia ruang',
		'pesan'				=> '',
		'pagination'		=> '',
		'tabel_data'		=> '',
		'main_view'			=> 'adm_sedia_ruang/sedia_ruang',
		'form_action'		=> '',
		'form_value'		=> '',
		'option_ruang'		=> '',
		'option_mulai'		=> '',
		'option_selesai'	=> '',
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_masterruang','ruang',TRUE);
		$this->load->model('model_mastersesi','sesi',TRUE);
		$this->load->model('model_admsediaruang','sedia',TRUE);
	}
	
	public function index($offset = 0)
	{
		$this->session->unset_userdata('id_sekarang','');
		
		$ruang = $this->sedia->cari_semua($offset);
		
		if($ruang)
		{
			$tabel = $this->sedia->buat_tabel($ruang);
			$this->data['tabel_data'] = $tabel;
			$this->data['pagination'] = $this->sedia->paging(site_url('adm_sediaruang/halaman'));
			
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada data ruang yang tersedia.';
		}
		$this->load->view('template_adm',$this->data);
	}
	
	public function tambah()
	{
		$this->data['breadcrumb']  	=	'Ketersediaan Ruang > Tambah';
		$this->data['main_view']   	= 	'adm_sedia_ruang/sedia_ruang_form';
		$this->data['form_action']	= 	'adm_sediaruang/tambah';

		$ruangan = $this->ruang->dd_ruang();
		if($ruangan)
		{
			foreach($ruangan as $row)
			{
				$this->data['option_ruang'][$row->idmRuang] = $row->ruang_nama;
			}
		}
		else
		{
			$this->data['option_ruang']['00'] = '-';
			$this->data['pesan'] = 'Data ruang tidak tersedia. Silahkan isi dahulu data ruangan';
		}
////////////////////////////////////////////////		
		$sesi_mulai = $this->sesi->cari_semua();
		if($sesi_mulai)
		{
			foreach($sesi_mulai as $row)
			{
				$this->data['option_mulai'][$row->idmSesi] = $row->sesi_mulai;
			}
		}
		else
		{
			$this->data['option_mulai'][00] = '-';
			$this->data['pesan'] = 'Data sesi tidak tersedia. Silahkan isi dahulu data sesi';
		}
//////////////////////////////////////////////////		
		$sesi_selesai = $this->sesi->cari_semua();
		if($sesi_selesai)
		{
			foreach($sesi_selesai as $row)
			{
				$this->data['option_selesai'][$row->idmSesi] = $row->sesi_selesai;
			}
		}
		else
		{
			$this->data['option_selesai'][00] = '-';
			$this->data['pesan'] = 'Data sesi tidak tersedia. Silahkan isi dahulu data sesi';
		}
///////////////////////////////////////////////////
		if($this->input->post('submit'))
		{
			if($this->sedia->validasi_tambah())
			{
				if($this->sedia->tambah())
				{
					$this->session->set_flashdata('pesan','Proses tambah data berhasil');
					redirect('adm_sediaruang');
				}
				else
				{
					$this->data['pesan'] = 'Proses tambah data gagal';
					$this->load->view('template_adm',$this->data);
				}
			}
			else
			{
				$this->load->view('template_adm',$this->data);
			}
		}
		else
		{
			$this->load->view('template_adm',$this->data);
		}
	}
	
	public function edit($idSediaRuang = NULL)
    {
        $this->data['breadcrumb']  = 'Sedia Ruang > Edit';
        $this->data['main_view']   = 'adm_sedia_ruang/sedia_ruang_form';
        $this->data['form_action'] = 'adm_sediaruang/edit/' . $idSediaRuang;

        $ruangan = $this->ruang->dd_ruang();
		if($ruangan)
		{
			foreach($ruangan as $row)
			{
				$this->data['option_ruang'][$row->idmRuang] = $row->ruang_nama;
			}
		}
		else
		{
			$this->data['option_ruang']['00'] = '-';
			$this->data['pesan'] = 'Data ruang tidak tersedia. Silahkan isi dahulu data ruangan';
		}
////////////////////////////////////////////////		
		$sesi_mulai = $this->sesi->cari_semua();
		if($sesi_mulai)
		{
			foreach($sesi_mulai as $row)
			{
				$this->data['option_mulai'][$row->idmSesi] = $row->sesi_mulai;
			}
		}
		else
		{
			$this->data['option_mulai'][00] = '-';
			$this->data['pesan'] = 'Data sesi tidak tersedia. Silahkan isi dahulu data sesi';
		}
//////////////////////////////////////////////////		
		$sesi_selesai = $this->sesi->cari_semua();
		if($sesi_selesai)
		{
			foreach($sesi_selesai as $row)
			{
				$this->data['option_selesai'][$row->idmSesi] = $row->sesi_selesai;
			}
		}
		else
		{
			$this->data['option_selesai'][00] = '-';
			$this->data['pesan'] = 'Data sesi tidak tersedia. Silahkan isi dahulu data sesi';
		}
///////////////////////////////////////////////////
		
		if( ! empty($idSediaRuang))
        {
            // submit
            if($this->input->post('submit'))
            {
                // validasi berhasil
                if($this->sedia->validasi_edit() === TRUE)
                {
                    //update db
                    $this->sedia->edit($idSediaRuang);
                    $this->session->set_flashdata('pesan', 'Proses update data berhasil.');

                    redirect('adm_sediaruang');
                }
                // validasi gagal
                else
                {
                    $this->load->view('template_adm', $this->data);
                }

            }
            // tidak disubmit, form pertama kali dimuat
            else
            {
                // ambil data dari database, $form_value sebagai nilai default form
                $tempat = $this->sedia->cari($idSediaRuang);
                foreach($tempat as $key => $value)
                {
                    $this->data['form_value'][$key] = $value;
                }

                // set temporary data untuk edit
                $this->session->set_userdata('id_sekarang', $tempat->idSediaRuang);

                $this->load->view('template_adm', $this->data);
            }
        }
        
        else
        {
            redirect('adm_sediaruang');
        }
    }
	
	public function hapus ($idSediaRuang = NULL)
	{
		if(! empty($idSediaRuang))
		{
			if($this->sedia->hapus($idSediaRuang))
			{
				$this->session->set_flashdata('pesan','Proses hapus data berhasil');
				redirect('adm_sediaRuang');
			}
			else
			{
				$this->session->set_flashdata('pesan','Proses hapus data gagal');
			}
		}
		else
		{
			$this->session->set_flashdata('pesan','Proses hapus data gagal');
			redirect('adm_sediaRuang');
		}
	}
	
	
}
