<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Master_mahasiswa extends MY_Controller
{
    public $data = array(

                        'breadcrumb'    => 'Master Mahasiswa',
                        'pesan'         => '',
                        'pagination'    => '',
                        'tabel_data'    => '',
                        'main_view'     => 'm_mahasiswa/mahasiswa',
                        'form_action'   => 'search_mhs/cari',
                        'form_value'    => '',
                        'option_prodi'  => '',
                         );

    public function __construct()
	{
		parent::__construct();		
		$this->load->model('model_mastermahasiswa', 'mahasiswa', TRUE);
		$this->load->model('model_masterprodi', 'prodi', TRUE);
	}

    public function index($offset = 0)
	{
        
        $this->session->unset_userdata('nim_sekarang', '');
        $mahasiswa = $this->mahasiswa->cari_semua($offset);
        if ($mahasiswa)
        {
            $tabel = $this->mahasiswa->buat_tabel($mahasiswa);
            $this->data['tabel_data'] = $tabel;

            $this->data['pagination'] = $this->mahasiswa->paging(site_url('mmahasiswa/halaman'));
        }
        // tidak ada data siswa
        else
        {
            $this->data['pesan'] = 'Tidak ada data mahasiswa.';
        }
        $this->load->view('template_adm', $this->data);
	}

    public function tambah()
    {
        $this->data['breadcrumb']  = 'Mahasiswa > Tambah';
        $this->data['main_view']   = 'm_mahasiswa/mahasiswa_form';
        $this->data['form_action'] = 'master_mahasiswa/tambah';

        $prodi = $this->prodi->cari_semua();

        if($prodi)
        {
            foreach($prodi as $row)
            {
                $this->data['option_prodi'][$row->idmProdi] = $row->prodi_nama;
            }
        }
        
        else
        {
            $this->data['option_prodi']['00'] = '-';
            $this->data['pesan'] = 'Data prodi tidak tersedia.';
        }

        if($this->input->post('submit'))
        {
            if($this->mahasiswa->validasi_tambah())
            {
                if($this->mahasiswa->tambah() && $this->mahasiswa->tambah_user())
                {
                    $this->session->set_flashdata('pesan', 'Proses tambah data berhasil.');
                    redirect('master_mahasiswa');
                }
                else
                {
                    $this->data['pesan'] = 'Proses tambah data gagal.';
                    $this->load->view('template_adm', $this->data);
                }
            }
        
            else
            {
                $this->load->view('template_adm', $this->data);
            }
        }
        
        else
        {
            $this->load->view('template_adm', $this->data);
        }
    }

    public function edit($idmmhsw = NULL)
    {
        $this->data['breadcrumb']  = 'Mahasiswa > Edit';
        $this->data['main_view']   = 'm_mahasiswa/mahasiswa_form';
        $this->data['form_action'] = 'master_mahasiswa/edit/' . $idmmhsw;

        // option kelas
        $prodi = $this->prodi->cari_semua();
        foreach($prodi as $row)
        {
            $this->data['option_prodi'][$row->idmProdi] = $row->prodi_nama;
        }

        if( ! empty($idmmhsw))
        {

            if($this->input->post('submit'))
            {

                if($this->mahasiswa->validasi_edit() === TRUE)
                {

                    $this->mahasiswa->edit($this->session->userdata('nim_sekarang'));
                    $this->session->set_flashdata('pesan', 'Proses update data berhasil.');

                    redirect('master_mahasiswa');
                }

                else
                {
                    $this->load->view('template_adm', $this->data);
                }

            }

            else
            {
                $mahasiswa = $this->mahasiswa->cari($idmmhsw);
                foreach($mahasiswa as $key => $value)
                {
                    $this->data['form_value'][$key] = $value;
                }

                $this->session->set_userdata('nim_sekarang', $mahasiswa->idmmhsw);

                $this->load->view('template_adm', $this->data);
            }
        }
        
        else
        {
            redirect('master_mahasiswa');
        }
    }

    public function hapus($idmmhsw = NULL)
    {
        if( ! empty($idmmhsw))
        {
            if($this->mahasiswa->hapus($idmmhsw))
            {
                $this->session->set_flashdata('pesan', 'Proses hapus data berhasil.');
                redirect('master_mahasiswa');
            }
            else
            {
                $this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
                redirect('master_mahasiswa');
            }
        }
        else
        {
            $this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
            redirect('master_mahasiswa');
        }
    }

    public function is_nim_exist()
    {
        $nim_sekarang  = $this->session->userdata('nim_sekarang');
        $nim_baru      = $this->input->post('mhsw_nim');

        if ($nim_baru === $nim_sekarang)
        {
            return TRUE;
        }
        else
        {
            // cek database untuk nis yang sama
            $query = $this->db->get_where('mmhsw', array('mhsw_nim' => $nim_baru));
            if($query->num_rows() > 0)
            {
                $this->form_validation->set_message('is_nim_exist',
                                                    "Siswa dengan NIM $nim_baru sudah terdaftar");
                return FALSE;
            }
            else
            {
                return TRUE;
            }
        }
    }

}
/* End of file siswa.php */
/* Location: ./application/controllers/siswa.php */