<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Adm_nilaiseminar extends MY_Controller
{
    public $data = array(
        'breadcrumb'    => 'Entri Nilai Seminar',
        'pesan'         => '',
        'pagination'    => '',
        'tabel_data'    => '',
        'main_view'     => 'adm_nilaiseminar/adm_nilaiseminar',
        'form_action'   => '',
        'form_value'    => '',
	
    );

	public function __construct()
	{	
		parent::__construct();		
		$this->load->model('model_adm_nilaiseminar', 'nilai', TRUE);
	}

	public function index($offset = 0)
    {
        $nilai = $this->nilai->cari_semua($offset);

        if ($nilai)
        {
            $tabel = $this->nilai->buat_tabel($nilai);
            $this->data['tabel_data'] = $tabel;
			$this->data['pagination'] = $this->nilai->paging(site_url('nilaiseminar/halaman'));
        }
        else
        {
            $this->data['pesan'] = 'Data sudah di verifikasi';
        }
        $this->load->view('template_adm', $this->data);
    }

    public function edit($idtSemSid)
    {
        $this->data['breadcrumb']  = 'Seminar > Entri Nilai';
        $this->data['main_view']   = 'adm_nilaiseminar/adm_nilaiseminar_form';
        $this->data['form_action'] = 'adm_nilaiseminar/edit/' . $idtSemSid;

        // pastikan parameter ada, mencegah error
        if( ! empty($idtSemSid))
        {
            // submit
            if($this->input->post('submit'))
            {
                // validasi berhasil
                if($this->nilai->validasi_edit() === TRUE)
                {
                    //update db
					$this->nilai->edit($idtSemSid);
					$this->ubah_status($idtSemSid);
					$this->session->set_flashdata('pesan', 'Proses entri nilai seminar berhasil. Sekarang isi status skripsinya!');
					redirect('adm_nilaiseminar/status_skripsi/'.$idtSemSid);
                }
                // validasi gagal
                else
                {
                    $this->load->view('template_adm', $this->data);
                }

            }
            // tidak disubmit, form pertama kali dimuat
            else
            {
                $absen = $this->nilai->cari($idtSemSid);
                foreach($absen as $key => $value)
                {
                    $this->data['form_value'][$key] = $value;
                }
               
                // set temporary data for edit
                $this->session->set_userdata('id_sekarang', $absen->idtSemSid);

                $this->load->view('template_adm', $this->data);
            }
        }
        // tidak ada parameter, kembalikan ke halaman absen
        else
        {
            redirect('adm_nilaiseminar');
        }
    }
	
	public function ubah_status($idtSemSid)
	{
		$mahasiswa = $this->nilai->cari_id($idtSemSid);
		foreach($mahasiswa as $row)
		{
			$row->idmSkripsi;
		}
		$this->nilai->edit_status($row->idmSkripsi);
	}
	
	public function status_skripsi($idtSemSid)
	{
		$this->data['breadcrumb']  = 'Status Skripsi > Ubah Status Skripsi';
        $this->data['main_view']   = 'adm_nilaiseminar/adm_status_form';
        $this->data['form_action'] = 'adm_nilaiseminar/status_skripsi/' . $idtSemSid;
		
		$skripsi = $this->nilai->SS();
		if($skripsi)
		{
			foreach($skripsi as $row)
			{
				$this->data['option_s'][$row->idmStatusSkrip] = $row->StatusSkrip_tipe;
			}
		}
		else
		{
			$this->data['option_s']['00'] = '-';
			$this->data['pesan'] = "Tidak ada data skripsi";
		}
		
		$mahasiswa = $this->nilai->cari_id($idtSemSid);
			foreach($mahasiswa as $row)
			{
				$row->idmSkripsi;
			}
		
		if($this->input->post('submit'))
		{
			$this->nilai->status_skripsi($row->idmSkripsi);
			$this->session->set_flashdata('pesan', 'Status skripsi berhasil berubah.');
			redirect('adm_nilaiseminar');
		}
		else
		{
			$this->load->view('template_adm', $this->data);
		}
	}
	
}

/* End of file model_adm_nilaiseminar.php */
/* Location: ./application/models/model_adm_nilaiseminar.php */