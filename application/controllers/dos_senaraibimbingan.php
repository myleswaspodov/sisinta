<?php if(!defined('BASEPATH')) exit ('No direct script acces allowed');

class Dos_senaraibimbingan extends MY_Controller
{
	public $data = array(
		'breadcrumb' => 'Pilih Data Mahasiswa Bimbingan',
		'tabel_data' => '',
		'main_view' => 'dos_senaraibimbingan/dos_senaraibimbinganawal',
		'pagination' => '',
		'pesan' => '',
		'mahasiswa' => '',
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_dos_senaraibimbingan','bimb',TRUE);
		$this->load->model('model_logbimbingan','log',TRUE);
	}
	
	public function index($offset = 0)
	{
		$this->load->view('template_dos',$this->data);
	}
	
	public function lihat_catatan_anak($idmhs)
	{
		$this->data['main_view'] = 'dos_senaraibimbingan/logbook_mhs';
		$this->data['mahasiswa'] = $this->log->logbook_mhs_dilist($idmhs);
		if(empty ($this->data['mahasiswa']) || is_null($this->data['mahasiswa']))
		{
			$this->data['pesan'] = 'Tidak ada catatan';
		}
		$this->load->view('template_dos',$this->data);
	}
	
	public function p1($offset = 0)
	{
		$this->data['main_view'] = 'dos_senaraibimbingan/dos_senaraibimbingan';
		$this->data['breadcrumb'] = 'Sebagai Pembimbing 1';
		$this->data['form_action'] = 'search_mhs_bimbingan/cari_p1';
		$bimbingan = $this->bimb->p1($offset);
		if($bimbingan)
		{
			$tabel = $this->bimb->buat_tabel($bimbingan);
			$this->data['tabel_data'] = $tabel;
			$this->data['pagination'] = $this->bimb->paging1(site_url('mahasiswa_bimbingan/halaman'));
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada mahasiswa bimbingan';
		}
		$this->load->view('template_dos',$this->data);
	}
	
	public function p2($offset = 0)
	{
		$this->data['main_view'] = 'dos_senaraibimbingan/dos_senaraibimbingan';
		$this->data['breadcrumb'] = 'Sebagai Pembimbing 2';
		$this->data['form_action'] = 'search_mhs_bimbingan/cari_p2';
		$bimbingan = $this->bimb->p2($offset);
		if($bimbingan)
		{
			$tabel = $this->bimb->buat_tabel($bimbingan);
			$this->data['tabel_data'] = $tabel;
			$this->data['pagination'] = $this->bimb->paging2(site_url('mahasiswa_bimbingan2/halaman'));
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada mahasiswa bimbingan';
		}
		$this->load->view('template_dos',$this->data);
	}
}

/* End of file dos_senaraibimbingan.php */
/* Location: ./application/controller/dos_senaraibimbingan.php */