<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Master_statusproses extends MY_Controller
{
    public $data = array(
                        'breadcrumb'    => 'Master Status Proses',
                        'pesan'         => '',
                        'pagination'    => '',
                        'tabel_data'    => '',
                        'main_view'     => 'm_statusProses/statusProses',
                        'form_action'   => '',
                        'form_value'    => '',
                         );

    public function __construct()
	{
		parent::__construct();		
		$this->load->model('model_masterstatusproses', 'SP', TRUE);
	}

    public function index()
	{
        // hapus data temporary proses update
        $this->session->unset_userdata('proses_sekarang', '');
  
        $SP = $this->SP->cari_semua();

        // ada data siswa, tampilkan
        if ($SP)
        {
            $tabel = $this->SP->buat_tabel($SP);
            $this->data['tabel_data'] = $tabel;

        }
        // tidak ada data siswa
        else
        {
            $this->data['pesan'] = 'Tidak ada data status.';
        }
        $this->load->view('template_adm', $this->data);
	}

    public function tambah()
    {
        $this->data['breadcrumb']  = 'Status > Tambah';
        $this->data['main_view']   = 'm_statusProses/statusProses_form';
        $this->data['form_action'] = 'master_statusproses/tambah';
		
        if($this->input->post('submit'))
        {
            // validasi sukses
            if($this->SP->validasi_tambah())
            {
                if($this->SP->tambah())
                {
                    $this->session->set_flashdata('pesan', 'Proses tambah data berhasil.');
                    redirect('master_statusproses');
                }
                else
                {
                    $this->data['pesan'] = 'Proses tambah data gagal.';
                    $this->load->view('template_adm', $this->data);
                }
            }
            // validasi gagal
            else
            {
                $this->load->view('template_adm', $this->data);
            }
        }
        // if no submit
        else
        {
            $this->load->view('template_adm', $this->data);
        }
    }

    public function edit($idmstatus_pros = NULL)
    {
        $this->data['breadcrumb']  = 'Status > Edit';
        $this->data['main_view']   = 'm_statusProses/statusProses_form';
        $this->data['form_action'] = 'master_statusproses/edit/' . $idmstatus_pros;

        if( ! empty($idmstatus_pros))
        {
            // submit
            if($this->input->post('submit'))
            {
                // validasi berhasil
                if($this->SP->validasi_edit() === TRUE)
                {
                    //update db
                    if($this->SP->edit($idmstatus_pros))
					{
						$this->session->set_flashdata('pesan', 'Proses update data berhasil.');
						redirect('master_statusproses');
					}
					else
					{
						$this->session->set_flashdata('pesan', 'Proses update data gagal! Id digunakan pada informasi lain!');
						redirect('master_statusproses');
					}
                }
                // validasi gagal
                else
                {
                    $this->load->view('template', $this->data);
                }

            }
            // tidak disubmit, form pertama kali dimuat
            else
            {
                // ambil data dari database, $form_value sebagai nilai default form
                $SP = $this->SP->cari($idmstatus_pros);
                foreach($SP as $key => $value)
                {
                    $this->data['form_value'][$key] = $value;
                }

        		$this->session->set_userdata('proses_sekarang', $SP->idmstatus_pros);

                $this->load->view('template_adm', $this->data);
            }
        }
        else
        {
            redirect('master_statusproses');
        }
    }

    public function hapus($idmstatus_pros = NULL)
    {
        if( ! empty($idmstatus_pros))
        {
            if($this->SP->hapus($idmstatus_pros))
            {
                $this->session->set_flashdata('pesan', 'Proses hapus data berhasil.');
                redirect('master_statusproses');
            }
            else
            {
                $this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
                redirect('master_statusproses');
            }
        }
        else
        {
            $this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
            redirect('master_statusproses');
        }
    }

}
/* End of file master_statusproses.php */
/* Location: ./application/controllers/master_statusproses.php */