<?php if(!defined('BASEPATH')) exit ('No direct script acces allowed');

class Master_afiliasi extends MY_Controller
{
	public $data = array (
			'breadcrumb'	=> 'Master Afiliasi',
			'pesan' 		=> '',
			'paginatiom'  	=> '',
			'tabel_data' 	=> '',
			'main_view' 	=> 'm_afiliasi/afiliasi',
			'form_action' 	=> '',
			'form_value' 	=> '',
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_masterafiliasi','afi',TRUE);
	}
	
	public function index($offset = 0)
	{
		$this->session->unset_userdata('id_sekarang');
		$afi = $this->afi->cari_semua($offset);
		
		if($afi)
		{
			$tabel = $this->afi->buat_tabel($afi);
			$this->data['tabel_data'] = $tabel;
			
			$this->data['pagination'] = $this->afi->paging(site_url('mafiliasi/halaman'));
		}
		else
		{
			$this->data['pesan'] = 'tidak ada data';
		}
		$this->load->view('template_adm',$this->data);
	}
	
	public function tambah()
	{
		$this->data['breadcrumb']	= 'Afiliasi > Tambah';
		$this->data['main_view']	= 'm_afiliasi/afiliasi_form'	;
		$this->data['form_action']	= 'master_afiliasi/tambah';
		
		if($this->input->post('submit'))
		{
			if($this->afi->validasi_tambah())
			{
				if($this->afi->tambah())
				{
					$this->session->set_flashdata('pesan','Proses tambah data berhasil');
					redirect('master_afiliasi');
				}
				else
				{
					$this->data['pesan'] = 'Proses tambah data gagal';
					$this->load->view('template_adm',$this->data);
				}
			}
			else
			{
				$this->load->view('template_adm',$this->data);
			}
		}
		else
		{
			$this->load->view('template_adm',$this->data);
		}
	}
	
	public function edit($idmAfiliasi = NULL)
	{
		$this->data['breadcrumb']		= 'Afiliasi > Edit';
		$this->data['main_view']		= 'm_afiliasi/afiliasi_form';
		$this->data['form_action']		= 'master_afiliasi/edit/' . $idmAfiliasi;
		if(!empty ($idmAfiliasi))
		{
			if($this->input->post('submit'))
			{
				if($this->afi->validasi_edit() === TRUE)
				{
					$this->afi->edit($this->session->userdata('id_sekarang'));
					$this->session->set_flashdata('pesan','Proses update data berhasil');
					redirect ('master_afiliasi');
				}
				else
				{
					$this->load->view('template_adm', $this->data);
				}
			}
			else
			{
				$afi = $this->afi->cari($idmAfiliasi);
				foreach($afi as $key => $value)
				{
					$this->data['form_value'][$key] = $value;
				}
				$this->session->set_userdata('id_sekarang', $afi->idmAfiliasi);
				$this->session->set_userdata('nama_sekarang', $afi->mAfiliasi_nama);
				$this->session->set_userdata('alamat_sekarang', $afi->afiliasi_alamat);
				$this->load->view('template_adm', $this->data);
			}
		}
		else
		{
			redirect ('master_afiliasi');
		}
	}
	
	public function hapus($idmAfiliasi = NULL)
	{
		if(!empty($idmAfiliasi))
		{
			if($this->afi->hapus($idmAfiliasi))
			{
				$this->session->set_flashdata('pesan','Proses hapus data berhasil.');
				redirect('master_afiliasi');
			}
			else
			{
				$this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
				redirect ('master_afiliasi');
			}
		}
		else
		{
			$this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
			redirect ('master_afiliasi');
		}
	}
	
	function id_exist()
	{
		$id_sekarang = $this->session->userdata('id_sekarang');
		$id_baru = $this->input->post('idmAfiliasi');
		
		if($id_baru === $id_sekarang)
		{
			return TRUE;
		}
		else
		{
			$query = $this->db->get_where('mafiliasi', array('idmAfiliasi' => $id_sekarang));
			if($query->num_rows() > 0)
			{
				$this->form_validation->set_message('id_exist',"ID dengan kode $id_baru sudah terdaftar");
				return FALSE;
			}
			else
			{
				return TRUE;
			}
			
		}
	}
	
}