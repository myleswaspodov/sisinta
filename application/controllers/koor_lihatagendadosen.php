<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Koor_lihatagendadosen extends MY_Controller
{
	public $data = array(
			'breadcrumb' => 'Agenda Dosen',
			'pagination' => '',
			'pesan'		=> '',
			'tabel_data' => '',
			'main_view'  => 'koor_agendadosen/agendadosen_koor'
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_koor_lihatagendadosen','agenda',TRUE);
	}
	
	public function index($offset = 0)
	{
		$agenda = $this->agenda->cari_semua($offset);
		if($agenda)
		{
			$tabel = $this->agenda->buat_tabel($agenda);
			$this->data['tabel_data'] = $tabel;
			$this->data['pagination'] = $this->agenda->paging(site_url('agendadosen/halaman'));
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada agenda dosen yang bisa ditampilkan';
		}
		$this->load->view('template_koor',$this->data);
	}
}