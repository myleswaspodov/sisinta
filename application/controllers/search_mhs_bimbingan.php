<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_mhs_bimbingan extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url'));
		$this->load->database();
		$this->load->library(array('pagination','session'));
		$this->load->model('model_dos_senaraibimbingan','data', TRUE);
	}
	
	function cari_p1()
	{
		$data['main_view'] = 'dos_senaraibimbingan/mhs_hasil';
		$data['form_action'] = 'search_mhs_bimbingan/cari_p1';
		
		$page=$this->uri->segment(3);
      	$batas=10;
		if(!$page):
		$offset = 0;
		else:
		$offset = $page;
		endif;	
		
		$data['nama']="";
		$postkata = $this->input->post('cari');
		if(!empty($postkata))
		{
			$data['nama'] = $this->input->post('cari');
			$this->session->set_userdata('pencarian_nama', $data['nama']);
		} 
		else 
		{
			$data['nama'] = $this->session->userdata('pencarian_nama');
		}
		$data['nama_nama'] = $this->data->cari_mhs1($batas,$offset,$data['nama']);
		$tot_hal = $this->data->total($data['nama']);
		
		$config['base_url'] = base_url() . 'index.php/search_mhs_bimbingan/cari_p1';
        	$config['total_rows'] = $tot_hal->num_rows();
        	$config['per_page'] = $batas;
			$config['uri_segment'] = 3;
	    	$config['first_link'] = '&#124;&lt; First';
			$config['last_link'] = 'Last &gt;&#124;';
			$config['next_link'] = 'Next &gt;';
			$config['prev_link'] = '&lt; Prev';
       		$this->pagination->initialize($config);
		$data["paginator"] = $this->pagination->create_links();
		
        $this->load->view('template_dos',$data);
	}
	
	function cari_p2()
	{
		$data['main_view'] = 'dos_senaraibimbingan/mhs_hasil_2';
		$data['form_action'] = 'search_mhs_bimbingan/cari_p2';
		
		$page=$this->uri->segment(3);
      	$batas=10;
		if(!$page):
		$offset = 0;
		else:
		$offset = $page;
		endif;	
		
		$data['nama']="";
		$postkata = $this->input->post('cari');
		if(!empty($postkata))
		{
			$data['nama'] = $this->input->post('cari');
			$this->session->set_userdata('pencarian_nama', $data['nama']);
		} 
		else 
		{
			$data['nama'] = $this->session->userdata('pencarian_nama');
		}
		$data['nama_nama'] = $this->data->cari_mhs2($batas,$offset,$data['nama']);
		$tot_hal = $this->data->total2($data['nama']);
		
		$config['base_url'] = base_url() . 'index.php/search_mhs_bimbingan/cari_p1';
        	$config['total_rows'] = $tot_hal->num_rows();
        	$config['per_page'] = $batas;
			$config['uri_segment'] = 3;
	    	$config['first_link'] = '&#124;&lt; First';
			$config['last_link'] = 'Last &gt;&#124;';
			$config['next_link'] = 'Next &gt;';
			$config['prev_link'] = '&lt; Prev';
       		$this->pagination->initialize($config);
		$data["paginator"] = $this->pagination->create_links();
		
        $this->load->view('template_dos',$data);
	}
	
	function cari_p1_koor()
	{
		$data['main_view'] = 'dos_senaraibimbingan/mhs_hasil_koor';
		$data['form_action'] = 'search_mhs_bimbingan/cari_p1_koor';
		
		$page=$this->uri->segment(3);
      	$batas=10;
		if(!$page):
		$offset = 0;
		else:
		$offset = $page;
		endif;	
		
		$data['nama']="";
		$postkata = $this->input->post('cari');
		if(!empty($postkata))
		{
			$data['nama'] = $this->input->post('cari');
			$this->session->set_userdata('pencarian_nama', $data['nama']);
		} 
		else 
		{
			$data['nama'] = $this->session->userdata('pencarian_nama');
		}
		$data['nama_nama'] = $this->data->cari_mhs1($batas,$offset,$data['nama']);
		$tot_hal = $this->data->total($data['nama']);
		
		$config['base_url'] = base_url() . 'index.php/search_mhs_bimbingan/cari_p1';
        	$config['total_rows'] = $tot_hal->num_rows();
        	$config['per_page'] = $batas;
			$config['uri_segment'] = 3;
	    	$config['first_link'] = '&#124;&lt; First';
			$config['last_link'] = 'Last &gt;&#124;';
			$config['next_link'] = 'Next &gt;';
			$config['prev_link'] = '&lt; Prev';
       		$this->pagination->initialize($config);
		$data["paginator"] = $this->pagination->create_links();
		
        $this->load->view('template_koor',$data);
	}
	
	function cari_p2_koor()
	{
		$data['main_view'] = 'dos_senaraibimbingan/mhs_hasil_koor_2';
		$data['form_action'] = 'search_mhs_bimbingan/cari_p2_koor';
		
		$page=$this->uri->segment(3);
      	$batas=10;
		if(!$page):
		$offset = 0;
		else:
		$offset = $page;
		endif;	
		
		$data['nama']="";
		$postkata = $this->input->post('cari');
		if(!empty($postkata))
		{
			$data['nama'] = $this->input->post('cari');
			$this->session->set_userdata('pencarian_nama', $data['nama']);
		} 
		else 
		{
			$data['nama'] = $this->session->userdata('pencarian_nama');
		}
		$data['nama_nama'] = $this->data->cari_mhs2($batas,$offset,$data['nama']);
		$tot_hal = $this->data->total2($data['nama']);
		
		$config['base_url'] = base_url() . 'index.php/search_mhs_bimbingan/cari_p1';
        	$config['total_rows'] = $tot_hal->num_rows();
        	$config['per_page'] = $batas;
			$config['uri_segment'] = 3;
	    	$config['first_link'] = '&#124;&lt; First';
			$config['last_link'] = 'Last &gt;&#124;';
			$config['next_link'] = 'Next &gt;';
			$config['prev_link'] = '&lt; Prev';
       		$this->pagination->initialize($config);
		$data["paginator"] = $this->pagination->create_links();
		
        $this->load->view('template_koor',$data);
	}
	
	function cari_p1_kaj()
	{
		$data['main_view'] = 'dos_senaraibimbingan/mhs_hasil_kaj';
		$data['form_action'] = 'search_mhs_bimbingan/cari_p1_kaj';
		
		$page=$this->uri->segment(3);
      	$batas=10;
		if(!$page):
		$offset = 0;
		else:
		$offset = $page;
		endif;	
		
		$data['nama']="";
		$postkata = $this->input->post('cari');
		if(!empty($postkata))
		{
			$data['nama'] = $this->input->post('cari');
			$this->session->set_userdata('pencarian_nama', $data['nama']);
		} 
		else 
		{
			$data['nama'] = $this->session->userdata('pencarian_nama');
		}
		$data['nama_nama'] = $this->data->cari_mhs1($batas,$offset,$data['nama']);
		$tot_hal = $this->data->total($data['nama']);
		
		$config['base_url'] = base_url() . 'index.php/search_mhs_bimbingan/cari_p1';
        	$config['total_rows'] = $tot_hal->num_rows();
        	$config['per_page'] = $batas;
			$config['uri_segment'] = 3;
	    	$config['first_link'] = '&#124;&lt; First';
			$config['last_link'] = 'Last &gt;&#124;';
			$config['next_link'] = 'Next &gt;';
			$config['prev_link'] = '&lt; Prev';
       		$this->pagination->initialize($config);
		$data["paginator"] = $this->pagination->create_links();
		
        $this->load->view('template_kajur',$data);
	}
	
	function cari_p2_kaj()
	{
		$data['main_view'] = 'dos_senaraibimbingan/mhs_hasil_kaj_2';
		$data['form_action'] = 'search_mhs_bimbingan/cari_p2_kaj';
		
		$page=$this->uri->segment(3);
      	$batas=10;
		if(!$page):
		$offset = 0;
		else:
		$offset = $page;
		endif;	
		
		$data['nama']="";
		$postkata = $this->input->post('cari');
		if(!empty($postkata))
		{
			$data['nama'] = $this->input->post('cari');
			$this->session->set_userdata('pencarian_nama', $data['nama']);
		} 
		else 
		{
			$data['nama'] = $this->session->userdata('pencarian_nama');
		}
		$data['nama_nama'] = $this->data->cari_mhs2($batas,$offset,$data['nama']);
		$tot_hal = $this->data->total2($data['nama']);
		
		$config['base_url'] = base_url() . 'index.php/search_mhs_bimbingan/cari_p1';
        	$config['total_rows'] = $tot_hal->num_rows();
        	$config['per_page'] = $batas;
			$config['uri_segment'] = 3;
	    	$config['first_link'] = '&#124;&lt; First';
			$config['last_link'] = 'Last &gt;&#124;';
			$config['next_link'] = 'Next &gt;';
			$config['prev_link'] = '&lt; Prev';
       		$this->pagination->initialize($config);
		$data["paginator"] = $this->pagination->create_links();
		
        $this->load->view('template_kajur',$data);
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */