<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Koor_lihatruang extends MY_controller
{
	public $data = array(
		'breadcrumb'		=> 'Ketersediaan Ruang',
		'pesan'				=> '',
		'tabel_data'		=> '',
		'pagination'		=> '',
		'main_view'			=> 'koor_ruang/ruang'
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_admsediaruang', 'tersedia', TRUE);
		$this->load->model('model_koor_lihatruang','ruang', TRUE);
		
	}
	
	public function index($offset = 0)
	{
		$ruangan = $this->tersedia->cari_semua($offset);
		if($ruangan)
		{
			$tabel = $this->ruang->buat_tabel($ruangan);
			$this->data['tabel_data'] = $tabel;
			$this->data['pagination'] = $this->tersedia->paging(site_url('ketersediaan_ruangan'));
		}
		else
		{
			$this->data['pesan']='Tidak ada data ruangan yang tersedia';
		}
		$this->load->view('template_koor',$this->data);
	}
}