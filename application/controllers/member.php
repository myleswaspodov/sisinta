<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_login', 'login', TRUE);
	}
	public function index()
	{
		if ($this->session->userdata('ugrup_idugrup') == 2)
		{
			redirect('all_katalogskripsi_kaj');
		}
		elseif ($this->session->userdata('ugrup_idugrup') == 3)
		{
			$this->login->cek_prodi();
			redirect('all_katalogskripsi_koor');
			
		}
		elseif ($this->session->userdata('ugrup_idugrup') == 4)
		{
			redirect('all_katalogskripsi_adm');
		}
		elseif ($this->session->userdata('ugrup_idugrup') == 5)
		{
			redirect('all_katalogskripsi_dos');
		}
		elseif ($this->session->userdata('ugrup_idugrup') == 6)
		{
			redirect('mhs_id');
		}
		else
		{
			redirect('login');
		}
		
	}
	
}

/* End of file member.php */
/* Location: ./application/controller/member.php */