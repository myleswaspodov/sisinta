<?php if(!defined('BASEPATH')) exit ('No direct script acces allowed');

class Adm_penjadwalanseminar extends MY_Controller
{
	public $data = array(
		'breadcrumb'	=> 'Penjadwalan Seminar',
		'main_view'		=> 'adm_penjadwalanseminar/jadwal',
		'pagination'	=> '',
		'tabel_data'	=> '',
		'form_value'	=> '',
		'form_action'	=> '',
		'pesan'			=> '',
		'opsi_mhs'		=> '',
		'opsi_ruang'	=> '',
		'jam'   		=> '',
		
	);	
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_adm_penjadwalanseminar','seminar',TRUE);
		$this->load->model('model_mastersesi','sesi',TRUE);
		$this->load->model('model_masterruang','ruang',TRUE);
	}
	
	public function index($offset = 0)
	{
		$data = $this->seminar->cari_semua($offset);
		if($data)
		{
			$tabel = $this->seminar->buat_tabel($data);
			$this->data['tabel_data'] = $tabel;
			$this->data['pagination']	= $this->seminar->paging(site_url('adm_seminar/halaman'));
		}
		else
		{
			$this->data['pesan'] = 'Belum ada data seminar';
			
		}
		$this->load->view('template_adm',$this->data);
	}
	
	public function tambah()
	{
		$this->data['main_view'] = 'adm_penjadwalanseminar/jadwal_form';
		$this->data['form_action']	= 'adm_penjadwalanseminar/tambah';
		$this->data['breadcrumb'] 	= 'Seminar > Penjadwalan';
		
		$jam = $this->sesi->cari_semua();
		if($jam)
		{
			foreach($jam as $row)
			{
				$this->data['jam'][$row->idmSesi] = $row->sesi_mulai;
			}
		}
		else
		{
			$this->data['jam']['00'] = '-';
			$this->data['pesan'] = 'data jam tidak ada';
		}
///////////////////////////////////////		
		$ruangan = $this->ruang->dd_ruang();
		if($ruangan)
		{
			foreach($ruangan as $row)
			{
				$this->data['opsi_ruang'][$row->idmRuang] = $row->ruang_nama;
				
			}
		}
		else
		{
			$this->data['opsi_ruang'][00]='-';
			$this->data['pesan'] = 'Data ruangan tidak tersedia. Silahkan isi dahulu data ruangan';
		}

////////////////////////////////////////
		$mhs = $this->seminar->dd_mhs();
		if($mhs)
		{
			foreach($mhs as $row)
			{
				$this->data['opsi_mhs'][$row->idmSkripsi] = $row->mhsw_nama;
			}
		}
		else
		{
			$this->data['opsi_mhs'][00] = '-';
			$this->data['pesan'] = 'Data mahasiswa tidak tersedia';
		}
		
////////////////////////////////////////
		if($this->input->post('submit'))
		{
			if($this->seminar->validasi_tambah())
			{
				if($this->seminar->tambah() && $this->seminar->edit_skripsi($this->input->post('idmSkripsi')))
				{
					$this->session->set_flashdata('pesan','Proses Penjadwalan Berhasil');
					redirect('adm_penjadwalanseminar');
				}
				else
				{
					$this->data['pesan'] = 'Proses Penjadwalan gagal';
					$this->load->view('template_adm', $this->data);
				}
			}
			else
			{
				$this->load->view('template_adm', $this->data);
			}
		}
		else
		{
			$this->load->view('template_adm',$this->data);
		}
	}
	
	public function edit($idtSemSid = NULL)
	{
		$this->data['main_view']	= 'adm_penjadwalanseminar/jadwal_edit';
		$this->data['form_action']	= 'adm_penjadwalanseminar/edit/'. $idtSemSid;
		$this->data['breadcrumb'] 	= 'Edit Jadwal Seminar';
		
///////////////////////////////////////		
		$jam = $this->sesi->cari_semua();
		if($jam)
		{
			foreach($jam as $row)
			{
				$this->data['jam'][$row->idmSesi] = $row->sesi_mulai;
			}
		}
		else
		{
			$this->data['jam']['00'] = '-';
			$this->data['pesan'] = 'data jam tidak ada';
		}
///////////////////////////////////////		
		$ruangan = $this->ruang->dd_ruang();
		if($ruangan)
		{
			foreach($ruangan as $row)
			{
				$this->data['opsi_ruang'][$row->idmRuang] = $row->ruang_nama;
				
			}
		}
		else
		{
			$this->data['opsi_ruang'][00]='-';
			$this->data['pesan'] = 'Data ruangan tidak tersedia. Silahkan isi dahulu data ruangan';
		}
////////////////////////////////////////
		if(!empty ($idtSemSid))
		{		
			if($this->input->post('submit'))
			{
				if($this->seminar->validasi_edit() === TRUE)
				{
					$this->seminar->edit($idtSemSid);
					$this->session->set_flashdata('pesan','Proses Edit Jadwal Seminar Berhasil');
					redirect('adm_penjadwalanseminar');
				}
				else
				{
					$this->load->view('template_adm', $this->data);
				}
			}
			else
			{
				$penjad = $this->seminar->cari($idtSemSid);
				foreach($penjad as $key=>$value)
				{
					$this->data['form_value'][$key] = $value;
				}
				$this->session->set_userdata('id_sekarang',$penjad->idtSemSid);
				$this->load->view('template_adm',$this->data);
			}
		}
		else
		{
			redirect('adm_penjadwalanseminar');
		}
	}
	
	public function hapus($idtSemSid = NULL)
	{
		$this->batal_seminar($idtSemSid);
	
		if($this->seminar->hapus($idtSemSid))
		{
			$this->session->set_flashdata('pesan','Proses Hapus Jadwal Seminar Berhasil');
			redirect('adm_penjadwalanseminar');
		}
		else
		{
			$this->session->set_flashdata('pesan','Proses Hapus jadwal seminar gagal');
			redirect('adm_penjadwalanseminar');
		}
		
	}
	 
	public function batal_seminar($id)
	{
		$batal = $this->seminar->get_idSk($id);
		$this->seminar->batal_seminar($batal);
	}
	
	public function is_format_tanggal($str)
    {
        if( ! preg_match('/(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-([0-9]{4})/', $str))
        {
            $this->form_validation->set_message('is_format_tanggal', 'Format tanggal tidak valid. (dd-mm-yyyy)');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
	
}
/* End of file adm_penjadwalanseminar.php */
/* Location: ./application/controller/adm_penjadwalanseminar.php */