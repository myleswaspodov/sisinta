<div id= "menu">
	<ul>	
		<li style="border-bottom:double;" align="center"><b>Menu Admin</b></li>
		<li><?php echo anchor('all_katalogskripsi_adm', 'Katalog Skripsi');?></li>
		<li><?php echo anchor('adm_input_nilai', 'Entri Nilai');?></li>
		<li><?php echo anchor('adm_afiliasi', 'Afiliasi Mahasiswa');?></li>
		<li><?php echo anchor('adm_sediaruang', 'Sedia Ruang');?></li>
		<li><?php echo anchor('adm_sediaruangkhusus', 'Sedia Ruang Khusus');?></li>
		<li><?php echo anchor('adm_penjadwalanseminar', 'Penjadwalan seminar');?></li>
		<li><?php echo anchor('adm_hakakses', 'Ganti Koordinator');?></li>
		<li><?php echo anchor('adm_hakakses_kajur', 'Ganti Kajur');?></li>
		<li><?php echo anchor('adm_admintandingan', 'Tambah Admin');?></li>
		<li><?php echo anchor('ganti_password_adm','Ganti Password');?></li>
		<li><?php echo anchor('adm_lupapasword','Pengguna');?></li>
		
		
	</ul>
</div>

<ul id="menucilik">	
	<li><?php echo anchor('all_katalogskripsi_adm', 'Katalog Skripsi');?></li>
    <li><?php echo anchor('adm_input_nilai', 'Entri Nilai');?></li>
	<li><?php echo anchor('adm_afiliasi', 'Afiliasi Mahasiswa');?></li>
    <li><?php echo anchor('adm_sediaruang', 'Sedia Ruang');?></li>
    <li><?php echo anchor('adm_sediaruangkhusus', 'Ruang Khusus');?></li>
	<li><?php echo anchor('adm_penjadwalanseminar', 'Penjadwalan seminar');?></li>
	<li><?php echo anchor('adm_hakakses', 'Ganti Koordinator');?></li>
	<li><?php echo anchor('adm_hakakses_kajur', 'Ganti Kajur');?></li>
	<li><?php echo anchor('adm_admintandingan', 'Tambah Admin');?></li>
	<li><?php echo anchor('ganti_password_adm','Ganti Password');?></li>
	<li><?php echo anchor('adm_lupapasword','Pengguna');?></li>
    <li><?php echo anchor('logout', 'Logout', array('onclick' => "return confirm('Anda yakin akan logout?')"));?></li>
</ul>