<?php
$form = array(
	'keterangan' => array(
			'name' => 'keterangan',
			'size'	=> '20',
			'class' => 'form_field',
			'value' => set_value('keterangan',isset($form_value['keterangan']) ? $form_value['keterangan']:'')
	),
    'submit'   => array(
        'name'=>'submit',
        'id'=>'submit',
        'value'=>'Simpan'
    )
);
?>

<h2><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php if (! empty($pesan)) : ?>
    <div class="pesan">
        <?php echo $pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- pesan flash message start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan flash message end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	
	<p>
		<?php echo form_label('Keterangan', 'keterangan'); ?>
		<?php echo form_textarea($form['keterangan']);?>
	</p>
	<?php form_error('keterangan','<p class="field_error">','</p>');?>
	
	<p>.
    <?php echo form_submit($form['submit']); ?>
    <?php echo anchor('koor_download/yg_sudahdidonlod','Batal', array('class' => 'cancel')) ?>
</p>
<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file adm_status_form.php */
/* Location: ./application/views/adm_status/adm_status_form.php */
?>