<?php
$form = array(
    'submit'   => array(
                        'name'=>'submit',
                        'id'=>'submit',
						'value'=>'Simpan'
                  )
    );
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan fg-white">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	<p class = "fg-white">
        <?php echo form_label('Instansi Afiliasi', 'idmAfiliasi'); ?>
        <?php echo form_dropdown('idmAfiliasi', $option_tempat, set_value('idmAfiliasi',isset($form_value['idmAfiliasi']) ? $form_value['idmAfiliasi'] : '')); ?>
	</p>
	<p><?php echo form_error('idmAfiliasi', '<p class="field_error fg-red">', '</p>');?></p>

	<p class = "fg-white">
        <?php echo form_label('Mahasiswa', 'idmSkripsi'); ?>
        <?php echo form_dropdown('idmSkripsi', $option_mhs, set_value('idmSkripsi',isset($form_value['idmSkripsi']) ? $form_value['idmSkripsi'] : '')); ?>
	</p>
	<p><?php echo form_error('idmSkripsi', '<p class="field_error fg-red">', '</p>');?></p>

	<p>
        <?php echo form_submit($form['submit']); ?>
        <?php echo anchor('adm_afiliasi','Batal'); ?>
	</p>
<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file adm_afiliasi_form.php */
/* Location: ./application/views/adm_afiliasi/adm_afiliasi_form.php */
?>