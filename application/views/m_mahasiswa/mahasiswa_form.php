<?php
$form = array(
    'mhsw_nim' => array(
        'name'=>'mhsw_nim',
        'size'=>'30',
        'class'=>'form_field',
        'value'=>set_value('mhsw_nim', isset($form_value['mhsw_nim']) ? $form_value['mhsw_nim'] : '')
    ),
    'mhsw_nama'    => array(
        'name'=>'mhsw_nama',
        'size'=>'30',
        'class'=>'form_field',
        'value'=>set_value('mhsw_nama', isset($form_value['mhsw_nama']) ? $form_value['mhsw_nama'] : '')
    ),
	'mhsw_tlp'    => array(
        'name'=>'mhsw_tlp',
        'size'=>'30',
        'class'=>'form_field',
        'value'=>set_value('mhsw_tlp', isset($form_value['mhsw_tlp']) ? $form_value['mhsw_tlp'] : '')
    ),    
    'submit'   => array(
        'name'=>'submit',
        'id'=>'submit',
        'value'=>'Simpan'
    )
);
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php if (! empty($pesan)) : ?>
    <div class="pesan">
        <?php echo $pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	<p class="fg-white">
        <?php echo form_label('NIM', 'mhsw_nim'); ?>
        <?php echo form_input($form['mhsw_nim']); ?>
	</p>
	<p><?php echo form_error('mhsw_nim', '<p class="field_error fg-red">', '</p>');?></p>
	
	<p class="fg-white">
        <?php echo form_label('Nama', 'mhsw_nama'); ?>
        <?php echo form_input($form['mhsw_nama']); ?>
	</p>
	<?php echo form_error('mhsw_nama', '<p class="field_error fg-red">', '</p>');?>	
	
	<p class="fg-white">
        <?php echo form_label('No Tlp', 'mhsw_tlp'); ?>
        <?php echo form_input($form['mhsw_tlp']); ?>
	</p>
	<?php echo form_error('mhsw_tlp', '<p class="field_error fg-red">', '</p>');?>	

	<p class="fg-white">
        <?php echo form_label('Prodi', 'idmProdi'); ?>
        <?php echo form_dropdown('idmProdi', $option_prodi, set_value('idmProdi', isset($form_value['idmProdi']) ? $form_value['idmProdi'] : '')); ?>
	</p>
	<?php echo form_error('idmProdi', '<p class="field_error fg-red">', '</p>');?>

	<p>
		<?php echo form_submit($form['submit']); ?>
        <?php echo anchor('master_mahasiswa','Batal', array('class' => 'cancel')) ?>
	</p>
<?php echo form_close(); ?>
<!-- form start -->

<?php
/* End of file mahasiswa_form.php */
/* Location: ./application/views/m_mahasiswa/mahasiswa_form.php */
?>