<?php
$form = array(
    'nilai_bimb1' => array(
        'name'=>'nilai_bimb1',
        'size'=>'30',
        'class'=>'form_field',
        'value'=>set_value('nilai_bimb1', isset($form_value['nilai_bimb1']) ? $form_value['nilai_bimb1'] : '')
    ),
	'nilai_bimb2' => array(
		'name' =>'nilai_bimb2',
		'size' => '30',
		'class'=>'form_field',
		'value' =>set_value('nilai_bimb2',isset($form_value['nilai_bimb2']) ? $form_value['nilai_bimb2'] : '')
	),
	'nilai_uji' => array(
		'name' =>'nilai_uji',
		'size' => 30,
		'class'=>'form_field',
		'value'=>set_value('nilai_uji',isset($form_value['nilai_uji']) ? $form_value['nilai_uji'] : '')
	),
	'nilai_uji2' => array(
		'name' =>'nilai_uji2',
		'size' => 30,
		'class'=>'form_field',
		'value'=>set_value('nilai_uji2',isset($form_value['nilai_uji2']) ? $form_value['nilai_uji2'] : '')
	),
    'submit'   => array(
        'name'=>'submit',
        'id'=>'submit',
        'value'=>'Simpan'
    )
);
?>

<h2><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php if (! empty($pesan)) : ?>
    <div class="pesan">
        <?php echo $pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
<p>
    <?php echo form_label('Nilai Pembimbing 1', 'nilai_bimb1'); ?>
    <?php echo form_input($form['nilai_bimb1']); ?>
</p>
<?php echo form_error('nilai_bimb1', '<p class="field_error">', '</p>');?>

<p>
    <?php echo form_label('Nilai Pembimbing 2', 'nilai_bimb2'); ?>
    <?php echo form_input($form['nilai_bimb2']); ?>
</p>
<?php echo form_error('nilai_bimb2', '<p class="field_error">', '</p>');?>

<p>
	<?php echo form_label('Nilai Penguji 1','nilai_uji');?>
	<?php echo form_input($form['nilai_uji']); ?>
</p>
<?php echo form_error('nilai_uji', '<p class="field_error">', '</p>');?>
<p>
	<?php echo form_label('Nilai Penguji 2','nilai_uji2');?>
	<?php echo form_input($form['nilai_uji2']); ?>
</p>
<?php echo form_error('nilai_uji2', '<p class="field_error">', '</p>');?>
<p>
    <?php echo form_submit($form['submit']); ?>
    <?php echo anchor('adm_nilaisidang','Batal', array('class' => 'cancel')) ?>
</p>
<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file adm_nilaisidang_form.php */
/* Location: ./application/views/adm_nilaisidang/adm_nilaisidang_form.php */
?>