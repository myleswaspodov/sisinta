<?php
$form = array(
    
	'skrip_abstrak' => array(
			'name' => 'skrip_abstrak',
			'rows' => '30',
			'cols' => '125',
			'class' => 'form_field',
			'value' => set_value('abstrak',isset($form_value['skrip_abstrak']) ? $form_value['skrip_abstrak'] : '')
	),
	
	'submit' => array(
                        'name'=>'submit',
                        'id'=>'submit',
						'value'=>'Simpan'
                  )
    );
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan fg-white">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- pesan start -->
<?php if (! empty($pesan)) : ?>
    <div class="pesan">
        <?php echo $pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<div class="pesan">
		Sebelum bisa melihat profil skripsi, isi dahulu abstrak skripsi Anda pada isian dibawah ini!
</div>

<!-- form start -->
<?php echo form_open($form_action); ?>
	<p>
		<?php echo form_textarea($form['skrip_abstrak']);?>
	</p>
	
	<?php echo form_error('skrip_abstrak', '<p class="field_error fg-red">', '</p>');?>	
	
	<p>
        <?php echo form_submit($form['submit']); ?>
	</p>
<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file prodi_form.php */
/* Location: ./application/views/prodi/prodi_form.php */
?>