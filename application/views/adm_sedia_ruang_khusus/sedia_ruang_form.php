<?php
$form = array(
    'srt_tgl'	=> array(
						'name'	=>'srt_tgl',
						'size'	=>'30',
						'class'	=>'form_field',
						'value'	=>set_value('srt_tgl',isset($form_value['srt_tgl'])? $form_value['srt_tgl']:''),
						'onclick' =>"displayDatePicker('srt_tgl')"
	),
	
	'submit'   	=> array(
                        'name'=>'submit',
                        'id'=>'submit',
						'value'=>'Simpan'
                  )
    );
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan fg-white">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	<p>
    	<?php echo form_label('Tanggal','srt_tgl');?>
        <?php echo form_input($form['srt_tgl']);?>
        <a href="javascript:void(0);" onclick="displayDatePicker('srt_tgl');"><img src="<?php echo base_url('asset/images/icon_calendar.png'); ?>" alt="calendar" border="0"></a>
    </p>
	<?php echo form_error('srt_tgl', '<p class="field_error">', '</p>');?>
	
	<p class = "fg-white">
        <?php echo form_label('Nama Ruangan', 'idmRuang'); ?>
        <?php echo form_dropdown('idmRuang', $option_ruang, set_value('idmRuang',isset($form_value['idmRuang']) ? $form_value['idmRuang'] : '')); ?>
	</p>
	<p><?php echo form_error('idmRuang', '<p class="field_error fg-red">', '</p>');?></p>

	<p class = "fg-white">
        <?php echo form_label('Jam Mulai', 'idmSesi_awal'); ?>
        <?php echo form_dropdown('idmSesi_awal', $option_mulai, set_value('idmSesi_awal',isset($form_value['idmSesi_awal']) ? $form_value['idmSesi_awal'] : '')); ?>
	</p>
	<p><?php echo form_error('idmSesi_awal', '<p class="field_error fg-red">', '</p>');?></p>

	<p class = "fg-white">
        <?php echo form_label('Jam Berakhir', 'idmSesi_akhir'); ?>
        <?php echo form_dropdown('idmSesi_akhir', $option_selesai, set_value('idmSesi_akhir',isset($form_value['idmSesi_akhir']) ? $form_value['idmSesi_akhir'] : '')); ?>
	</p>
	<p><?php echo form_error('idmSesi_akhir', '<p class="field_error fg-red">', '</p>');?></p>

	<p>
        <?php echo form_submit($form['submit']); ?>
        <?php echo anchor('adm_sediaruangkhusus','Batal', array('class' => 'cancel')) ?>
	</p>
<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file prodi_form.php */
/* Location: ./application/views/prodi/prodi_form.php */
?>