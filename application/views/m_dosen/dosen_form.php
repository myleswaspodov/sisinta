<?php
$form = array(
    'dos_nip' => array(
                        'name'=>'dos_nip',
                        'size'=>'30',
                        'class'=>'form_field',
                        'value'=>set_value('dos_nip', isset($form_value['dos_nip']) ? $form_value['dos_nip'] : '')
                  ),
    'dos_nama'    => array(
                        'name'=>'dos_nama',
                        'size'=>'30',
                        'class'=>'form_field',
                        'value'=>set_value('dos_nama', isset($form_value['dos_nama']) ? $form_value['dos_nama'] : '')
                  ),
	'dos_sandi' => array(
						'name' => 'dos_sandi',
						'size' => '7',
						'class' =>'form_field',
						'value' => set_value('dos_sandi',isset($form_value['dos_sandi'])? $form_value['dos_sandi']:'' )
				  ),
    'submit'   => array(
                        'name'=>'submit',
                        'id'=>'submit',
						'value'=>'Simpan'
                  )
    );
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan fg-white">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	<p class = "fg-white">
        <?php echo form_label('NIP', 'dos_nip'); ?>
        <?php echo form_input($form['dos_nip']); ?>
	</p>
	<p><?php echo form_error('dos_nip', '<p class="field_error fg-red">', '</p>');?></p>
	
	<p class = "fg-white">
        <?php echo form_label('Nama Dosen', 'dos_nama'); ?>
        <?php echo form_input($form['dos_nama']); ?>
	</p>
	<p><?php echo form_error('dos_nama', '<p class="field_error fg-red">', '</p>');?></p>

	<p class = "fg-white">
        <?php echo form_label('Sandi Dosen', 'dos_sandi'); ?>
        <?php echo form_input($form['dos_sandi']); ?>
	</p>
	<p><?php echo form_error('dos_sandi', '<p class="field_error fg-red">', '</p>');?></p>

	<p class="fg-white">
        <?php echo form_label('Prodi Dosen', 'dos_prodi'); ?>
        <?php echo form_dropdown('dos_prodi', $prodi, set_value('dos_prodi', isset($form_value['dos_prodi']) ? $form_value['dos_prodi'] : '')); ?>
	</p>
	<?php echo form_error('dos_prodi', '<p class="field_error fg-red">', '</p>');?>

	
	<p>
        <?php echo form_submit($form['submit']); ?>
        <?php echo anchor('master_dosen','Batal', array('class' => 'cancel')) ?>
	</p>
<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file dosen_form.php */
/* Location: ./application/views/prodi/dosen_form.php */
?>