<!DOCTYPE>
<body>
<?php echo form_open($form_action); ?>

<div id="katalog">
	<?php echo anchor('adm_lupapasword','Lihat Semua');?>
</div>

Kata Kunci Pencarian : 
<br />
<input type="text" name="cari" size="50" autocomplete="off" placeholder="ketikkan NIM / NIP" />
<button type="submit">Cari</button>

<?php echo form_close(); ?>

<table id="zebra">
<?php
	echo '<tr> <th>Username</th><th>Pengguna</th><th>Aksi</th></tr>';
	if(count($pengguna->result_array())>0)
	{
		foreach($pengguna->result_array() as $nd)
		{
			echo '<tr class="zebra"><td>'.$nd['users_name'].'</td>';
			echo '<td>'.$nd['user_nama'].'</td>';
			echo '<td>'.anchor('adm_lupapasword/normal/'.$nd['users_name'].'/'.$nd['users_name'],'reset',array('class' => 'pass','onclick'=>"return confirm('Anda yakin akan mengganti password user ini?')")).' '.
			anchor('adm_lupapasword/hapus/'.$nd['users_name'],'hapus',array('class' => 'delete','onclick'=>"return confirm('Anda yakin akan menghapus user ini?')")).'</td>';
			
			
		}
	}
	else
	{
		echo '<tr><td>Tidak ditemukan pengguna dengan NIM / NIP <b>"'.$user.'"</b></td></tr>';
	}
?>

</table>
<div id="pagination"><?php echo $paginator; ?>
</div>
</body>