<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/coor.css');?>" />
<!-- pesan flash message start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan flash message end -->

<!-- pesan start -->
<?php if (! empty($pesan)) : ?>
    <div class="pesan">
        <?php echo $pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<?php
	foreach($lihat as $row):
?>

<div>
	<?php echo anchor('koor_dataskripsi/diterima/'.$row->idmSkripsi,'Verifikasi',array('class' => 'oyi','onclick'=>"return confirm('Verifikasi judul skripsi ini?')"));?>
	<?php echo anchor('koor_dataskripsi/ditolak/'.$row->idmSkripsi,'Unverifikasi',array('class' => 'tolak','onclick'=>"return confirm('Status judul skripsi tidak terverifikasi?')"));?>
	|| <?php echo anchor('koor_dataskripsi/edit/'.$row->idmSkripsi,'edit',array('class' => 'edit'));?>
	<?php echo anchor('koor_dataskripsi/hapus/'.$row->idmSkripsi,'hapus',array('class' => 'delete','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')"));?>
	<div id="katalog"><?php echo anchor('koor_dataskripsi','Kembali')?></div>
</div>
<br>
<hr>
<?php
	$hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->skrip_tgl_reg));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->skrip_tgl_reg));
            $hr_tgl = "$hari, $tgl";
?>
<div>
	<table>
	<tr>
		<td colspan="2" align="center"><b><?php echo $row->skrip_judul;?></b></td>
	</tr>
	<tr></tr><tr></tr>
	<tr>
		<td><p>Status Judul </p></td>
		<td> : <?php if($row->status_judul == 1)
		{
			echo "Judul Diterima";
		}
		elseif(($row->status_judul == 0))
		{
			echo "Judul Menunggu Konfirmasi";
		}
		else
		{
			echo "Judul ditolak";
		}
		?><br/></td>
	</tr>
	<tr>
		<td><p>Tgl Registrasi </p></td>
		<td> : <?php echo $hr_tgl;?><br/></td>
	</tr>
	<tr>
		<td><p>Nama Mahasiswa </p></td>
		<td> : <?php echo $row->mhsw_nama;?><br/></td>
	</tr>
	<tr>
		<td><p>NIM </p></td>
		<td> : <?php echo $row->mhsw_nim;?><br/></td>
	</tr>
	<tr>
		<td><p>Prodi </p></td>
		<td> : <?php echo $row->prodi_nama;?><br/></td>
	</tr>
	<tr>
		<td><p>Status Skripsi </p></td>
		<td> : <?php echo $row->StatusSkrip_tipe;?><br/></td>
	</tr>
	<tr>
		<td><p>Status Proses </p></td>
		<td> : <?php echo $row->statuspros_tipe;?><br/></td>
	</tr>
	<tr>
		<td><p>Pembimbing 1 </p></td>
		<td> : <?php echo $row->p1;?><br/></td>
	</tr>
	<tr>
		<td><p>Pembimbing 2</p></td>
		<td> : <?php echo $row->p2;?><br/></td>
	</tr>
	<table>
	<div>
		<p>Abstrak Skripsi</p>
		<p><?php echo $row->skrip_abstrak;?></p>
	</div>
</div>
<?php
	endforeach;
?>

<?php
/* End of file adm_afiliasi.php */
/* Location: ./application/views/adm_afiliasi/adm_afiliasi.php */
?>