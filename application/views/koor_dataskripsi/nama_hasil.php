<!DOCTYPE>
<body>

<?php echo form_open($form_action); ?>
<div id="katalog">
	<?php echo anchor('koor_dataskripsi','Lihat Semua');?>
</div>

Cari Mahasiswa : 
<br />
<input type="text" name="cari" size="50" autocomplete="off" placeholder="ketikkan nama" />
<button type="submit">Cari</button>

<table id="zebra">
<?php
	$banyak = count($nama_nama->result_array());
	echo '<tr><td>Ditemukan <b>'.$banyak.'</b> hasil pencarian dengan kata <b>"'.$nama.'"</b></td></tr>';
	echo '<tr> <th>Judul Skripsi</th><th>NIM</th><th>Mahasiswa</th><th>Jurusan</th><th>Pembimbing1</th><th>Pembimbing2</th></tr>';
	
	if(count($nama_nama->result_array())>0)
	{
		foreach($nama_nama->result_array() as $nd)
		{
			echo '<tr class="zebra"><td>'.$nd['skrip_judul'].'</td>';
			echo '<td>'.$nd['mhsw_nim'].'</td>';
			echo '<td>'.$nd['mhsw_nama'].'</td>';
			echo '<td>'.$nd['prodi_nama'].'</td>';
			echo '<td>'.$nd['p1'].'</td>';
			echo '<td>'.$nd['p2'].'</td>';
			echo '<td>'.anchor('koor_dataskripsi/detail/'.$nd['idmSkripsi'],'proses',array('class' => 'setting')).'</td>';
		}
	}
	else
	{
		echo '<tr><td>Tidak ditemukan mahasiswa dengan kata kunci <b>"'.$nama.'"</b></td></tr>';
	}
?>

</table>
<div id="pagination"><?php echo $paginator; ?>
</div>
</body>