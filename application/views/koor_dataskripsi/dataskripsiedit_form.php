<?php
$form = array(
   'submit'   	=> array(
                        'name'=>'submit',
                        'id'=>'submit',
						'value'=>'Simpan'
                  )
    );
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php if (! empty($pesan)) : ?>
    <div class="pesan">
        <?php echo $pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- pesan start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan fg-white">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	<p class = "fg-white">
        <?php echo form_label('Pembimbing 1', 'idmDosen_Bi1'); ?>
        <?php echo form_dropdown('idmDosen_Bi1', $opsi_p1, set_value('idmDosen_Bi1',isset($form_value['idmDosen_Bi1']) ? $form_value['idmDosen_Bi1'] : '')); ?>
	</p>
	<p><?php echo form_error('idmDosen_Bi1', '<p class="field_error fg-red">', '</p>');?></p>

	<p class = "fg-white">
        <?php echo form_label('Pembimbing 2', 'idmDosen_Bi2'); ?>
        <?php echo form_dropdown('idmDosen_Bi2', $opsi_p2, set_value('idmDosen_Bi2',isset($form_value['idmDosen_Bi2']) ? $form_value['idmDosen_Bi2'] : '')); ?>
	</p>
	<p><?php echo form_error('idmDosen_Bi2', '<p class="field_error fg-red">', '</p>');?></p>

	<p class = "fg-white">
        <?php echo form_label('Kategori 1', 'idmKategori1'); ?>
        <?php echo form_dropdown('idmKategori1', $opsi_k1, set_value('idmKategori1',isset($form_value['idmKategori1']) ? $form_value['idmKategori1'] : '')); ?>
	</p>
	<p><?php echo form_error('idmKategori1', '<p class="field_error fg-red">', '</p>');?></p>

	<p class = "fg-white">
        <?php echo form_label('Kategori 2', 'idmKategori2'); ?>
        <?php echo form_dropdown('idmKategori2', $opsi_k2, set_value('idmKategori2',isset($form_value['idmKategori2']) ? $form_value['idmKategori2'] : '')); ?>
	</p>
	<p><?php echo form_error('idmKategori2', '<p class="field_error fg-red">', '</p>');?></p>
	
	<p>
        <?php echo form_submit($form['submit']); ?>
        <?php echo anchor('koor_dataskripsi','Batal', array('class' => 'cancel')) ?>
	</p>
<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file prodi_form.php */
/* Location: ./application/views/prodi/prodi_form.php */
?>