<?php
$form = array(
    'idmRuang' => array(
                        'name'=>'idmRuang',
                        'size'=>'5',
                        'class'=>'form_field',
                        'value'=>set_value('idmRuang', isset($form_value['idmRuang']) ? $form_value['idmRuang'] : '')
                  ),
    'ruang_nama'    => array(
                        'name'=>'ruang_nama',
                        'size'=>'25',
                        'class'=>'form_field',
                        'value'=>set_value('ruang_nama', isset($form_value['ruang_nama']) ? $form_value['ruang_nama'] : '')
                  ),
    'submit'   => array(
                        'name'=>'submit',
                        'id'=>'submit',
						'value'=>'Simpan'
                  )
    );
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan fg-white">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	<p class = "fg-white">
        <?php echo form_label('Kode Ruangan', 'idmRuang'); ?>
        <?php echo form_input($form['idmRuang']); ?>
	</p>
	<p><?php echo form_error('idmRuang', '<p class="field_error fg-red">', '</p>');?></p>
	
	<p class = "fg-white">
        <?php echo form_label('Nama Ruangan', 'ruang_nama'); ?>
        <?php echo form_input($form['ruang_nama']); ?>
	</p>
	<p><?php echo form_error('ruang_nama', '<p class="field_error fg-red">', '</p>');?></p>

	<p>
        <?php echo form_submit($form['submit']); ?>
        <?php echo anchor('master_ruang','Batal', array('class' => 'cancel')) ?>
	</p>
<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file prodi_form.php */
/* Location: ./application/views/prodi/prodi_form.php */
?>