<!DOCTYPE>
<body>
<?php echo form_open($form_action); ?>

<div id="katalog"> 
	<?php echo anchor('all_katalogskripsi_adm','Lihat Semua');?>
</div>

Kata Kunci Pencarian : 
<br />
<input type="text" name="cari" size="50" autocomplete="off" placeholder="ketikkan judul" />
<button type="submit">Cari Judul</button>

<?php echo form_close(); ?>


<table id="zebra">
<?php
	$banyak = count($katalog->result_array());
	echo '<tr><td>Ditemukan <b>'.$banyak.'</b> hasil pencarian dengan kata <b>"'.$judul.'"</b></td></tr>';
	echo '<tr> <th>Judul</th><th>Nim</th><th>Nama</th><th>Prodi</th></tr>';
	
	$att = array(
			'width' => '480',
			'height' => '480',
			'scroolbars' => 'no',
			'status' => 'yes',
			'resizable' => 'no',
			'screenx' => '500',
			'screeny' => '80',
			'class'=> 'detail'
		);
	
	if(count($katalog->result_array())>0)
	{
		foreach($katalog->result_array() as $nd)
		{
			echo '<tr class="zebra"><td>'.$nd['skrip_judul'].'</td>';
			echo '<td>'.$nd['mhsw_nim'].'</td>';
			echo '<td>'.$nd['mhsw_nama'].'</td>';
			echo '<td>'.$nd['prodi_nama'].'</td>';
			echo '<td>'.anchor_popup('all_katalogskripsi/detail/'.$nd['idmSkripsi'],'Detail',$att).'</td>';
		}
	}
	else
	{
		echo '<tr><td>Tidak ditemukan skripsi dengan kata kunci <b>"'.$judul.'"</b></td></tr>';
	}
?>

</table>
<div id="pagination"><?php echo $paginator; ?>
</div>

</body>